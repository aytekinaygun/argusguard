<?php

include './resource/Veritabani.php';

if ($_GET) {
    $id = htmlspecialchars($_GET["id"]);
    if ($id >= 0) {
        $query = $Connection->prepare('delete from inputfirewall where id = :id;');
        $query->execute(array('id' => $id));
    }
}
$Connection = NULL;
header('Location:InputFirewalls.php');
exit();
?>