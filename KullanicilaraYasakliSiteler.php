<!DOCTYPE html>
<html lang="tr">
    <head>
        <?php
        include './resource/MetaTitleLink.php';
        include './resource/Veritabani.php';
        include './resource/dosyaYollari.php';
        ?> 
    </head>
    <body>
    <div id="wrapper">
        <?php
        $mSelect = 52;
        include './resource/NavBar.php';
        ?> 
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Tüm Kullanıcılara Yasak Siteler</h1>
                </div>
            </div>
            <!--BURADAN SONRA İÇERİK GİRİLEBİLİR-->

            <div class="row">
                <div class="col-lg-8">
                    <table class="table table-striped table-bordered table-hover" id="grdIzinli">
                        <thead>
                            <tr>
                                <th>Siteler</th>
                                <th style="width: 30px"></th>
                                <th style="width: 30px"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $fh = fopen($DansguardianKlasoru . 'lists/bannedsitelist-all', 'r+');
                            while (!feof($fh)) {
                                $lSatir = fgets($fh);
                                if (strlen(trim($lSatir)) > 0 && $lSatir[1] != '#') {
                                    $lAktif = !($lSatir[0] == '#');

                                    if (!$lAktif)
                                        $lSatir = substr($lSatir, 1);

                                    echo
                                    '<tr> ' .
                                    '   <td ' . ((!$lAktif) ? 'style="text-decoration:line-through;"' : '') . '>' . $lSatir . '</td> ' .
                                    '   <td> ' .
                                    '       <button onclick="FormuAc(\'' . trim($lSatir) . '\',\'' . trim($lSatir) . '\',' . ($lAktif ? 'true' : 'false') . ');" type="button" ' .
                                    '               class="btn btn-outline btn-primary btn-xs"><i class="fa fa-edit"></i></button> ' .
                                    '   </td> ' .
                                    '   <td> ' .
                                    '       <button onclick="SilFormuAc(\'' . trim($lSatir) . '\');" type="button" ' .
                                    '               class="btn btn-outline btn-danger btn-xs"><i class="fa fa-times"></i></button> ' .
                                    '   </td> ' .
                                    '</tr>';
                                }
                            }
                            fclose($fh);
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-3">
                    <button type="button" class="btn btn-outline btn-info" onclick="FormuAc('', 'Yeni Site', 'true');" >
                        <i class="fa fa-plus"></i> Kayıt Ekle
                    </button>
                    <br/><br/>Siteleri 'www' veya 'http' kullanmadan giriniz.URL girmeyiniz.<br/><br/><b>Örnek:</b><br/>google.com<br/>personel.saglik.gov.tr<br/>gov.tr<br/>edu.tr
                </div>
            </div>

            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Site Bilgisi</h4>
                        </div> 
                        <form id = "f1" name = "f1" role = "form" action = "KullanicilaraYasakliSitelerKaydet.php" method = "POST" >
                            <div class="modal-body">
                                <div class = "row">
                                    <div class = "col-lg-12">
                                        <div class = "form-group">
                                            <label>Site Adı</label>
                                            <input id="name" name = "name" class = "form-control" />
                                            <input id="oldName" name = "oldName" type = "hidden"  />
                                        </div>
                                        <div class = "form-group">
                                            <label><input id="aktif" name="aktif" type="checkbox" /> Aktif</label>
                                        </div>
                                    </div>    
                                </div>    
                            </div>
                            <div class="modal-footer">
                                <button type="submit"  class="btn btn-primary" ><i class="fa fa-save"></i> Kaydet</button>
                                <button type = "button" class = "btn btn-default" data-dismiss = "modal">İptal</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="frmSil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Dikkat</h4>
                        </div>
                        <form id = "f2" name = "f2" role = "form" action = "KullanicilaraYasakliSitelerKaydet.php" method = "POST" >
                            <div class="modal-body">
                                Kaydı [<span id="aName"></span>] silmek istediğinizden emin misiniz?
                                <input id="silName" name = "silName" type = "hidden"  />
                            </div>
                            <div class="modal-footer">
                                <button type="submit"  class="btn btn-danger" ><i class="fa fa-times"></i> Sil</button>
                                <button type = "button" class = "btn btn-default" data-dismiss = "modal">İptal</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <button id="btnFormShow" type="button"  data-toggle="modal" data-target="#myModal" style="display: none;" ></button>
            <button id="btnSilFormShow" type="button"  data-toggle="modal" data-target="#frmSil" style="display: none;" ></button>
            <!--BURADAN ÖNCE İÇERİK GİRİLEBİLİR-->
        </div>
    </div>
    <?php include './resource/EndScript.php'; ?>  
    <script>
        $(document).ready(function () {
            $('#grdIzinli').dataTable();
        });
        function FormuAc(aOldName, aValue, aAktif) {
            $('#name').val(aValue);
            $('#oldName').val(aOldName);
            $('#aktif').prop("checked", aAktif);
            $('#btnFormShow').click();
        }
        function SilFormuAc(aValue) {
            $('#aName').html(aValue);
            $('#silName').val(aValue);
            $('#btnSilFormShow').click();
        }
    </script>
</body>

</html>
