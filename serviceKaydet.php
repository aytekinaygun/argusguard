<?php

include './resource/Veritabani.php';

if ($_POST) {
    $id = htmlspecialchars($_POST["id"]);
    $name = htmlspecialchars($_POST["name"]);
    $port = htmlspecialchars($_POST["port"]);
    $protokol = htmlspecialchars($_POST["protokol"]);
    $comment = htmlspecialchars_decode($_POST["comment"]);


    if ($id == 0) {
        $query = $Connection->prepare('INSERT INTO service ( tur, name, port, protokol, comment) 
                                        VALUES ( :tur, :name, :port, :protokol, :comment);');
        $query->execute(array('tur' => 2, 'name' => $name, 'port' => $port, 'protokol' => $protokol, 'comment' => $comment));
    } else {
        $query = $Connection->prepare('UPDATE service SET name=:name, port=:port, protokol=:protokol, comment=:comment WHERE id = :id;');
        $query->execute(array( 'name' => $name, 'port' => $port, 'protokol' => $protokol, 'comment' => $comment, 'id' => $id));
    }
    
    $Connection = null;
}
header('Location:services.php');
exit();
?>