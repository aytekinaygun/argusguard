<?php

$dsn = 'mysql:host=localhost;dbname=argusguard';
$user = 'root';
$password = 'aaa';

try {
    $Connection = new PDO($dsn, $user, $password);
    $Connection->exec('SET NAMES UTF8');
} catch (PDOException $e) {
    echo 'Bağlantı Hatası: ' . $e->getMessage();
}

function DbBaglanHazir() {
    return DbBaglan('mysql:host=localhost;dbname=4UsAdmin', 'root', '');
}

function DbBaglan($dns, $user, $pass) {
    try {
        $BaglanilanVeritabaniBaglantisi = new PDO($dns, $user, $pass);
        $BaglanilanVeritabaniBaglantisi->exec('SET NAMES UTF8');
        return $BaglanilanVeritabaniBaglantisi;
    } catch (PDOException $e) {
        echo '<script> alert("Bağlantı Hatası: ' . $e->getMessage() . '"); </script>';
        return null;
    }
}
