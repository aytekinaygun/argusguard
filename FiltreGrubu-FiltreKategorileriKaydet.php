<?php

include './resource/Veritabani.php';
include './resource/dosyaYollari.php';

function DosyayaYaz($db, $id, $dsg, $field, $iy, $dosyaAdi) {
    $DosyaIcerigi = "";
    foreach ($db->query('SELECT ' . $field . ' FROM filtre_kategori '
            . 'where id in (select kategori from filtre_kategori_secim where izin_yasak = ' . $iy . ' and grup = ' . $id . ')') as $row) {
        $DosyaIcerigi .= $row[$field] . "\r\n";
    }
    file_put_contents($dsg . "/lists/filtergroups" . $id . "/" . $dosyaAdi, $DosyaIcerigi);
}

function Guncelle($db, $id, $kategori, $durum) {
    $row = $db->query(' select count(*) say ' .
                    '     from filtre_kategori_secim ' .
                    '    where kategori = ' . $kategori . ' and grup = ' . $id)->fetch();
    if ($row['say'] == 0) {
        $db->exec('INSERT INTO filtre_kategori_secim (grup,izin_yasak,kategori)VALUES(' . $id . ',' . $durum . ',' . $kategori . ');');
    } else {
        $db->exec('update filtre_kategori_secim set izin_yasak = ' . $durum . ' where kategori = ' . $kategori . ' and grup = ' . $id);
    }
}

if ($_GET) {
    $id = htmlspecialchars_decode($_GET["id"]);
    $num = htmlspecialchars_decode($_GET["n"]);
    $islem = htmlspecialchars_decode($_GET["i"]);

    switch ($islem) {
        case 20: Guncelle($Connection, $id, $num, 0);
            break;
        case 21:Guncelle($Connection, $id, $num, 1);
            break;
        case 22:Guncelle($Connection, $id, $num, 2);
            break;
    }

    DosyayaYaz($Connection, $id, $DansguardianKlasoru, "domain", 1, "exceptionsitelist-kategori");
    DosyayaYaz($Connection, $id, $DansguardianKlasoru, "domain", 2, "bannedsitelist-kategori");
    DosyayaYaz($Connection, $id, $DansguardianKlasoru, "url", 1, "exceptionurllist-kategori");
    DosyayaYaz($Connection, $id, $DansguardianKlasoru, "url", 2, "bannedurllist-kategori");
}
header('Location:FiltreGrubu-FiltreKategorileri.php?id=' . $id);
exit();
?>