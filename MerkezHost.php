<!DOCTYPE html>
<html lang="tr">
    <head>
        <?php include './resource/MetaTitleLink.php'; ?> 
    </head>
    <body>
    <div id="wrapper">
        <?php
        $mSelect = 12;
        include './resource/NavBar.php';
        include './resource/Veritabani.php';

        $id = 0;
        $name = "";
        $address = "";
        $netmask = "32";
        $comment = "";
        if ($_GET) {
            $id = $_GET['id'];

            $row = $Connection->query('SELECT * FROM host WHERE id = ' . $id)->fetch();
            if ($row) {
                $id = $row["id"];
                $name = $row["name"];
                $address = $row["address"];
                $netmask = $row["netmask"];
                $comment = $row["comment"];
            } else {
                header('Location:MerkezHosts.php');
                exit();
            }
        }
        ?> 
        <div id="page-wrapper">
            <br/>
            <!--BURADAN SONRA İÇERİK GİRİLEBİLİR-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="background-color: #8BC7F2;">
                            Merkezi Host Bilgisi
                            <div style="text-align: right; position: static; float: right">
                                <?php
                                if ($id > 0)
                                    echo '<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#myModal">Hostu Sil</button>';
                                ?>
                            </div>
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Dikkat</h4>
                                        </div>
                                        <div class="modal-body">
                                            Merkezi Host kaydını silmek istediğinizden emin misiniz?
                                        </div>
                                        <div class="modal-footer">

                                            <a href = "MerkezHostKaydet.php?silId=<?php echo $id; ?>">
                                                <button type = "button" class = "btn btn-danger" >Sil</button>
                                            </a>
                                            <button type = "button" class = "btn btn-default" data-dismiss = "modal">Hayır</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class = "panel-body">
                            <div class = "row">
                                <form id = "form1" name = "form1" role = "form" action = "MerkezHostKaydet.php" method = "POST" onsubmit = "return IpAdresKontrol(document.form1.address)">

                                    <div class = "col-lg-6">
                                        <input name = "id" type = "hidden" value = "<?php echo $id . ''; ?>" />
                                        <div class = "form-group">
                                            <label>Name</label>
                                            <input name = "name" class = "form-control" placeholder = "Name" value = "<?php echo $name ?>"/>
                                        </div>
                                        <div class = "form-group">
                                            <label>Address</label>
                                            <input name = "address" class = "form-control" placeholder = "0.0.0.0" value = "<?php echo $address; ?>">
                                        </div>
                                    </div>                                    
                                    <div class = "col-lg-6">
                                        <div class = "form-group">
                                            <label>Net Mask</label>
                                            <select name = "netmask" class = "form-control" >
                                                <?php
                                                for ($i = 1; $i <= 32; $i++) {
                                                    echo '<option' . ($i == $netmask ? ' selected = "selected"' : '') . '>' . $i . '</option>';
                                                }
                                                ?>                                                
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Comment</label>
                                            <input name="comment" class="form-control" placeholder="Comment" value="<?php echo $comment; ?>">
                                        </div>
                                    </div>
                                    <br/>
                                    <button type="submit" style=" float: right; margin-right: 15px; width: 120px" class="btn btn-primary" ><i class="fa fa-save"></i> Kaydet</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--BURADAN ÖNCE İÇERİK GİRİLEBİLİR-->
        </div>
    </div>
    <?php include './resource/EndScript.php'; ?> 
    <script>
        function IpAdresKontrol(inputText)
        {
            var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
            if (inputText.value.match(ipformat))
            {
                return true;
            }
            else
            {
                alert("Adsress bilgisi geçerli değil");
                return false;
            }
        }
    </script>
</body>

</html>
