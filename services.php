<!DOCTYPE html>
<html lang="tr">
    <head>
        <?php
        include './resource/MetaTitleLink.php';
        include './resource/Veritabani.php';
        ?> 
    </head>
    <body>
    <div id="wrapper">
        <?php 
        $mSelect = 31;
        include './resource/NavBar.php'; 
        ?> 
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Servis Kayıtları
                        <a href="service.php">
                            <button type="button" style="float: right" class="btn btn-outline btn-info"><i class="fa fa-plus"></i> Servis Ekle</button>
                        </a>
                    </h1>
                </div>
            </div>
            <!--BURADAN SONRA İÇERİK GİRİLEBİLİR-->

            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>Servis Adı</th>
                                    <th>Port</th>
                                    <th>Protokol</th>
                                    <th>Açıklama</th>
                                    <th style="width: 30px"> </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($Connection->query('SELECT * FROM service where tur = 2') as $row) {
                                    echo '
                                <tr>
                                    <td>' . $row['name'] . '</td>
                                    <td>' . $row['port'] . '</td>
                                    <td>' . $row['protokol'] . '</td>
                                    <td>' . $row['comment'] . '</td>
                                    <td>
                                        <a href="service.php?id=' . $row['id'] . '">
                                        <button type="button" class="btn btn-outline btn-primary btn-xs"><i class="fa fa-edit"></i></button>
                                        </a>
                                    </td>
                                </tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--BURADAN ÖNCE İÇERİK GİRİLEBİLİR-->
        </div>
    </div>
    <?php include './resource/EndScript.php'; ?>  
    <script>
        $(document).ready(function () {
            $('#dataTables-example').dataTable();
        });
    </script>
</body>

</html>
