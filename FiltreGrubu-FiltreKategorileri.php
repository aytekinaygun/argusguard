<!DOCTYPE html>
<html lang="tr">
    <head>
        <?php
        include './resource/MetaTitleLink.php';
        include './resource/dosyaYollari.php';
        include './resource/DosyaIslemleri.php';
        include './resource/Veritabani.php';
        include './resource/Araclar.php';

        function DosyayaYaz($db, $id, $dsg, $field, $iy, $dosyaAdi) {
            $DosyaIcerigi = "";
            foreach ($db->query('SELECT ' . $field . ' FROM filtre_kategori '
                    . 'where id in (select kategori from filtre_kategori_secim where izin_yasak = ' . $iy . ' and grup = ' . $id . ')') as $row) {
                $DosyaIcerigi .= $row[$field] . "\r\n";
            }
            file_put_contents($dsg . "lists/filtergroups" . $id . "/" . $dosyaAdi, $DosyaIcerigi);
        }

        function Guncelle($db, $id, $kategori, $durum) {
            $row = $db->query(' select count(*) say ' .
                            '     from filtre_kategori_secim ' .
                            '    where kategori = ' . $kategori . ' and grup = ' . $id)->fetch();
            if ($row['say'] == 0) {
                $db->exec('INSERT INTO filtre_kategori_secim (grup,izin_yasak,kategori)VALUES(' . $id . ',' . $durum . ',' . $kategori . ');');
            } else {
                $db->exec('update filtre_kategori_secim set izin_yasak = ' . $durum . ' where kategori = ' . $kategori . ' and grup = ' . $id);
            }
        }

        $id = GETT('id', 0);
        $num = GETT('n', -1);
        $islem = GETT('i', -1);
        if ($id == 0) {
            header('Location:FiltreGruplari.php');
            exit();
        }
        if ($islem > -1) {
            switch ($islem) {
                case 20: Guncelle($Connection, $id, $num, 0);
                    break;
                case 21: Guncelle($Connection, $id, $num, 1);
                    break;
                case 22: Guncelle($Connection, $id, $num, 2);
                    break;
            }
            DosyayaYaz($Connection, $id, $DansguardianKlasoru, "domain", 1, "exceptionsitelist-kategori");
            DosyayaYaz($Connection, $id, $DansguardianKlasoru, "domain", 2, "bannedsitelist-kategori");
            DosyayaYaz($Connection, $id, $DansguardianKlasoru, "url", 1, "exceptionurllist-kategori");
            DosyayaYaz($Connection, $id, $DansguardianKlasoru, "url", 2, "bannedurllist-kategori");
        }
        $GrupAdi = GrupAdiGetir($DansguardianKlasoru, $id);
        ?> 
    </head>
    <body>
    <div id="wrapper">
        <?php
        $mSelect = 53;
        include './resource/NavBar.php';
        ?> 
        <div id="page-wrapper">
            <!--BURADAN SONRA İÇERİK GİRİLEBİLİR-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="panel panel-default" style="margin-bottom: 15px; margin-top: -20px;">
                            <div class="panel-heading">
                                <h3 style="margin-bottom: 0;"><?php echo $GrupAdi; ?></h3>
                            </div>
                            <div class="panel-body" style="padding: 3px;">
                                <?php
                                $FiltreGrubuMenu = 4;
                                include './FiltreGrubuMenu.php';
                                ?>   
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="row">
                                <div  class="panel panel-info">
                                    <div class="panel-heading">
                                        Filtre Kategorileri
                                    </div>
                                    <div class="panel-body" style="margin-top:-51px">
                                        <table class="table table-striped table-bordered table-hover" id="grdListe" >
                                            <thead>
                                                <tr>
                                                    <th style="width: 40px; text-align: center">İzinli</th>
                                                    <th style="width: 40px; text-align: center">Yasaklı</th>
                                                    <th>Kategori</th>
                                                    <th>Açıklama</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach ($Connection->query(
                                                        '    SELECT f.id, f.kategori, f.aciklama, coalesce(s.izin_yasak ,0) izin_yasak ' .
                                                        '      FROM filtre_kategori f ' .
                                                        ' left join filtre_kategori_secim s on s.kategori = f.id and s.grup = ' . $id .
                                                        '  order by f.kategori asc ') as $row) {
                                                    $ButonIzinliNo = $row["izin_yasak"] == 1 ? "20" : "21";
                                                    $ButonYasakliNo = $row["izin_yasak"] == 2 ? "20" : "22";
                                                    $ButonIzinli = $row["izin_yasak"] == 1 ? "" : "btn-outline";
                                                    $ButonYasakli = $row["izin_yasak"] == 2 ? "" : "btn-outline";
                                                    $Pasif = $row["izin_yasak"] == 0 ? "style=\"color: #aaaaaa;\"" : "";
                                                    echo
                                                    '<tr> ' .
                                                    '   <td style=" text-align: center"> ' .
                                                    '       <a href="FiltreGrubu-FiltreKategorileri.php?id=' . $id . '&n=' . $row["id"] . '&i=' . $ButonIzinliNo . '" ' .
                                                    '          class="btn ' . $ButonIzinli . '  btn-primary btn-xs"><i class="fa fa-check"></i>' .
                                                    '       </a> ' .
                                                    '   </td> ' .
                                                    '   <td style=" text-align: center"> ' .
                                                    '       <a href="FiltreGrubu-FiltreKategorileri.php?id=' . $id . '&n=' . $row["id"] . '&i=' . $ButonYasakliNo . '" ' .
                                                    '          class="btn ' . $ButonYasakli . '  btn-primary btn-xs"><i class="fa fa-check"></i>' .
                                                    '       </a> ' .
                                                    '   </td> ' .
                                                    '   </td> ' .
                                                    '   <td ' . $Pasif . '> ' . $row["kategori"] . '   </td> ' .
                                                    '   <td ' . $Pasif . '> ' . $row["aciklama"] . '   </td> ' .
                                                    '</tr>';
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--BURADAN ÖNCE İÇERİK GİRİLEBİLİR-->
        </div>
    </div>
    <?php include './resource/EndScript.php'; ?> 
    <script>
        $(document).ready(function () {
            $('#grdListe').dataTable({
                ordering: false,
                paging: false
            });
        });
    </script>
</body>

</html>
