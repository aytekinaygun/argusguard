<?php

include './resource/Veritabani.php';

if ($_POST) {
    $id = htmlspecialchars($_POST["id"]);
    $name = htmlspecialchars($_POST["name"]);
    $comment = htmlspecialchars_decode($_POST["comment"]);


    if ($id == 0) {
        if ($Connection->exec('INSERT INTO hostgroup (tur, name, comment) VALUES (2, "' . $name . '", "' . $comment . '")')) {
            $id = $Connection->lastInsertId();
            $Connection = null;
            header('Location:group.php?id=' . $id);
            exit();
        }
    } else {
        $query = $Connection->prepare('UPDATE hostgroup SET name=:name, comment=:comment WHERE id = :id;');
        $query->execute(array('name' => $name, 'comment' => $comment, 'id' => $id));
    }
    $Connection = null;
}
header('Location:groups.php');
exit();
?>