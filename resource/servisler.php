<?php

$servisListesi = array(
    array("adi" => "İçerik Filtreleme Servisi",
        "durum" => "sudo /usr/local/sbin/is_running.py DANSGUARDIAN",
        "start" => "sudo /usr/local/sbin/dansguardian_restart.sh&&sudo backup-restore-argusguard.py backup"),
    array("adi" => "Güvenlik Duvarı Kuralları",
        "durum" => "sudo /usr/local/sbin/is_running.py DANSGUARDIAN",
        "start" => "sudo /usr/local/sbin/xml2iptables.py&&sudo /usr/local/sbin/firewall-argusguard.sh&&sudo backup-restore-argusguard.py backup"),
);
