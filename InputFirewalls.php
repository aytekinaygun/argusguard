<!DOCTYPE html>
<html lang="tr">
    <head>
        <?php
        include './resource/MetaTitleLink.php';
        include './resource/Veritabani.php';
        ?> 
    </head>
    <body>
    <div id="wrapper">
        <?php
        $mSelect = 43;
        include './resource/NavBar.php';
        ?> 
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Güvenlik Duvarına Gelen Kurallar
                        <a href="InputFirewall.php">
                            <button type="button" style="float: right" class="btn btn-outline btn-info"><i class="fa fa-plus"></i> Kayıt Ekle</button>
                        </a>
                    </h1>
                </div>
            </div>
            <!--BURADAN SONRA İÇERİK GİRİLEBİLİR-->

            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>if</th>
                                    <th>Kaynak</th>
                                    <th>Servis</th>
                                    <th>Kural</th>
                                    <th>Açıklama</th>
                                    <th style="width: 60px"> </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($Connection->query('        
         SELECT il.*,
		CASE WHEN il.sGroup = 1 THEN
			(SELECT g.name FROM hostgroup g WHERE g.id = il.sName)
		ELSE
			(SELECT CONCAT(h.name,\' <span class="ipbil">\' ,h.address,\'</span>\') FROM host h WHERE h.id = il.sName)
		END sNameAdi,
		(SELECT CONCAT(h.name,\' <span class="ipbil">\' ,h.port,\'</span> \' ,h.protokol)  FROM service h WHERE h.id = il.service) serviceAdi
           FROM inputfirewall il ') as $row) {
                                    echo '
                                <tr>
                                    <td>' . $row['id'] . '</td>
                                    <td>' . $row['if'] . '</td>
                                    <td><img src="resource/img/' . $row['sGroup'] . '.png"/> ' . $row['sNameAdi'] . '</td>
                                    <td>' . $row['serviceAdi'] . '</td>
                                    <td style="color:' . (($row['policy'] == "ACCEPT") ? '#009933' : '#EF0D16') . '" >' . $row['policy'] . '</td>
                                    <td>' . $row['comment'] . '</td>
                                    <td>
                                        <a href="InputFirewall.php?id=' . $row['id'] . '">
                                        <button type="button" class="btn btn-outline btn-primary btn-xs"><i class="fa fa-edit"></i></button>
                                        </a>
<ul class="nav navbar-top-links navbar-right" >
    <div class="btn-group" >
        <button type="button" btn-outline class="btn dropdown-toggle btn-outline btn-info btn-xs " data-toggle="dropdown"><span class="caret"></span></button>
        <ul class="dropdown-menu" role="menu" style="min-width:0px; margin-top:-43px; margin-right:-12px; padding: 5px 3px;">    
<a style="margin: 1px 5px" href="FirewallKaydir.php?id=' . $row['id'] . '&yon=1&sayfa=3" class="btn btn-outline btn-info btn-xs" ><i class="fa fa-chevron-up"></i></a>
<a style="margin: 1px 5px" href="FirewallKaydir.php?id=' . $row['id'] . '&yon=2&sayfa=3" class="btn btn-outline btn-info btn-xs" ><i class="fa fa-chevron-down"></i></a>
        </ul>
    </div>
</ul>
                                    </td>
                                </tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--BURADAN ÖNCE İÇERİK GİRİLEBİLİR-->
        </div>
    </div>
    <?php include './resource/EndScript.php'; ?>  
    <script>
        $(document).ready(function () {
            $('#dataTables-example').dataTable({
                paging: false
            });
        });
    </script>
</body>

</html>
