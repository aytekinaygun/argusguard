<!DOCTYPE html>
<html lang="tr">
    <head>
        <?php
        include './resource/MetaTitleLink.php';
        include './resource/dosyaYollari.php';
        include './resource/DosyaIslemleri.php';
        include './resource/Veritabani.php';
        include './resource/Araclar.php';

        $id = GETT('id', 0);
        $tur = GETT('tur', 0);
        $kul = GETT('k', 0);
        $sil = GETT('s', 0);
        if ($kul > 0) {
            $say = $Connection->query('select count(*) say from filtre_kullanicilar where filtre_grubu=' . $id . ' and kullanici=' . $kul . ' and tur=' . $tur)->fetch();
            if (($say) && ($say['say'] == 0)) {
                $Connection->exec('INSERT INTO filtre_kullanicilar(filtre_grubu,kullanici,tur) VALUES(' . $id . ',' . $kul . ',' . $tur . ');');
            }
        }
        if ($sil > 0) {
            $Connection->exec('delete from filtre_kullanicilar where id=' . $sil);
        }
        if ($kul > 0 || $sil > 0) {
            include './FiltreGrubuKullanicilarDosyasiOlustur.php';
        }

        $GrupAdi = "";
        if ($id > 0)
            $GrupAdi = GrupAdiGetir($DansguardianKlasoru, $id);
        ?> 
    </head>
    <body>
    <div id="wrapper">
        <?php
        $mSelect = 53;
        include './resource/NavBar.php';
        ?> 
        <div id="page-wrapper">
            <!--BURADAN SONRA İÇERİK GİRİLEBİLİR-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="panel panel-default" style="margin-bottom: 15px; margin-top: -20px;">
                            <div class="panel-heading">
                                <h3 style="margin-bottom: 0;"><?php echo $GrupAdi; ?></h3>
                            </div>
                            <div class="panel-body" style="padding: 3px;">
                                <?php
                                $FiltreGrubuMenu = 9;
                                include './FiltreGrubuMenu.php';
                                ?>   
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="row">
                                <div  class="panel panel-info">
                                    <div class="panel-heading">
                                        Filtre Grubu Kullanicilar
                                        <div style="margin: -7px 0px; float: right;" >
                                            <button type="button" style="width: 142px;" class="btn btn-outline btn-info" data-toggle="modal" 
                                                    data-target="#HostList"><i class="fa fa-plus"></i> Yeni Host Ekle</button>
                                            <!--<button type="button" style="width: 142px;" class="btn btn-outline btn-info" data-toggle="modal"--> 
                                                    <!--data-target="#GroupList"><i class="fa fa-plus"></i> Yeni Group Ekle</button>-->
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <table class="table table-striped table-bordered table-hover" id="grdListe" >
                                            <thead>
                                                <tr>
                                                    <th>Host / Group</th>
                                                    <th>Açıklama</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach ($Connection->query(
                                                        '   SELECT k.*, ' .
                                                        '	   CASE WHEN k.tur = 1 THEN g.name ELSE CONCAT(h.name,\' <span class="ipbil">\' ,h.address,\' / \',h.netmask,\'</span>\') END kullanini_adi, ' .
                                                        '	   CASE WHEN k.tur = 1 THEN g.comment ELSE h.comment END kullanini_aciklama ' .
                                                        '     FROM filtre_kullanicilar k ' .
                                                        'LEFT JOIN host h ON k.tur = 0 AND h.id = k.kullanici ' .
                                                        'LEFT JOIN hostgroup g ON k.tur = 1 AND g.id = k.kullanici ' .
                                                        '    WHERE k.filtre_grubu = ' . $id) as $row) {
                                                    echo
                                                    '<tr> ' .
                                                    '   <td><img src="resource/img/' . $row['tur'] . '.png"/> ' . $row["kullanini_adi"] . '   </td> ' .
                                                    '   <td>' . $row["kullanini_aciklama"] . '   </td> ' .
                                                    '   <td style=" text-align: center"> ' .
                                                    '       <a onClick="SilFormuAc(' . $row['id'] . ');" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a> ' .
                                                    '   </td> ' .
                                                    '</tr>';
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="HostList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button id="HostKapat" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Host Listesi</h4>
                        </div>
                        <div class="modal-body">
                            <table class="table table-striped table-bordered table-hover" id="grdHostList">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>name</th>
                                        <th>ip</th>
                                        <th>netmask</th>
                                        <th>comment</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($Connection->query(
                                            'SELECT id, name, address, comment, netmask FROM host WHERE id NOT IN ' .
                                            '(SELECT kullanici FROM filtre_kullanicilar WHERE tur = 0 AND filtre_grubu = ' . $id . ')') as $row) {
                                        echo '
                                            <tr style="cursor: pointer" onClick="Gitt(' . $id . ',0,' . $row['id'] . ');">
                                                <td>' . $row['id'] . '</td>
                                                <td>' . $row['name'] . '</td>
                                                <td>' . $row['address'] . '</td>
                                                <td>' . $row['netmask'] . '</td>
                                                <td>' . $row['comment'] . '</td>
                                            </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="GroupList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button id="GroupKapat" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Group Listesi</h4>
                        </div>
                        <div class="modal-body">
                            <table class="table table-striped table-bordered table-hover" id="grdGroupList">
                                <thead>
                                    <tr>
                                        <th style="width: 10px">id</th>
                                        <th>name</th>
                                        <th>comment</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($Connection->query('select id, name, comment from hostgroup WHERE id NOT IN ' .
                                            '(SELECT kullanici FROM filtre_kullanicilar WHERE tur = 1 AND filtre_grubu = ' . $id . ')') as $row) {
                                        echo '
                                            <tr style="cursor: pointer" onClick="Gitt(' . $id . ',1,' . $row['id'] . ');">
                                                <td>' . $row['id'] . '</td>
                                                <td>' . $row['name'] . '</td>
                                                <td>' . $row['comment'] . '</td>                                           
                                            </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="frmSil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Dikkat</h4>
                        </div>
                        <form id = "f2" name = "f2" role = "form" action = "FiltreGrubu-Kullanicilar.php" method = "GET" >
                            <div class="modal-body">
                                <input id="Id" name = "id" type = "hidden" value="<?php echo $id . ""; ?>" />
                                Kaydı silmek istediğinizden emin misiniz?
                                <input id="s" name = "s" type = "hidden"  />
                            </div>
                            <div class="modal-footer">
                                <button type="submit"  class="btn btn-danger" ><i class="fa fa-times"></i> Sil</button>
                                <button type="button" class = "btn btn-default" data-dismiss = "modal">İptal</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <button id="btnFrmSil" type="button"  data-toggle="modal" data-target="#frmSil" style="display: none;" ></button>
            <script>
                function SilFormuAc(aValue) {
                    $('#s').val(aValue);
                    $('#btnFrmSil').click();
                }
            </script>

            <!--BURADAN ÖNCE İÇERİK GİRİLEBİLİR-->
        </div>
    </div>
    <?php include './resource/EndScript.php'; ?> 
    <script>
        $(document).ready(function () {
            $('#grdListe').dataTable();
            $('#grdHostList').dataTable();
            $('#grdGroupList').dataTable();
        });
        function Gitt(aId, aTur, aKullanici) {
            window.location = 'FiltreGrubu-Kullanicilar.php?id=' + aId + '&tur=' + aTur + '&k=' + aKullanici;
        }
    </script>
</body>

</html>
