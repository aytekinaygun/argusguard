<?php

include './resource/Veritabani.php';

if ($_POST) {
    $id = htmlspecialchars($_POST["id"]);
    $if = htmlspecialchars_decode($_POST["if"]);
    $dGroup = htmlspecialchars_decode($_POST["dGroup"]);
    $dName = htmlspecialchars_decode($_POST["dName"]);
    $sGroup = htmlspecialchars_decode($_POST["sGroup"]);
    $sName = htmlspecialchars_decode($_POST["sName"]);
    $service = htmlspecialchars_decode($_POST["service"]);
    $policy = htmlspecialchars_decode($_POST["policy"]);
    $comment = htmlspecialchars_decode($_POST["comment"]);


    if ($id == 0) {
        $query = $Connection->prepare('INSERT INTO `lantointernet`
            (`comment`,`dGroup`,`dName`,`if`,`policy`,`service`,`sGroup`,`sName`) VALUES
            (:comment,:dGroup,:dName,:if,:policy,:service,:sGroup,:sName);
            ');
        $query->execute(array('comment' => $comment, 'dGroup' => $dGroup, 'dName' => $dName, 'if' => $if, 'policy' => $policy,
            'service' => $service, 'sGroup' => $sGroup, 'sName' => $sName));
    } else {
        $query = $Connection->prepare('
            UPDATE `lantointernet`
            SET
            `comment` = :comment,
            `dGroup` = :dGroup,
            `dName` = :dName,
            `if` = :if,
            `policy` = :policy,
            `service` = :service,
            `sGroup` = :sGroup,
            `sName` = :sName
            WHERE `id` = :id;
            ');
        $query->execute(array('comment' => $comment, 'dGroup' => $dGroup, 'dName' => $dName, 'if' => $if, 'policy' => $policy,
            'service' => $service, 'sGroup' => $sGroup, 'sName' => $sName, 'id' => $id));
    }
}
$Connection = NULL;
header('Location:lanToInternets.php');
exit();
