<html>
    <head>
        <?php include './resource/MetaTitleLink.php'; ?> 
    </head>
    <body>
        <?php
        include './resource/Veritabani.php';
        include './resource/Araclar.php';

        session_start();
        if (empty($_SESSION['oturum'])) {
            header('Location:giris.php');
            exit();
        }


        if ($_POST) {
            $eParola = trim(POSTT("eParola", ""));
            $yParola1 = trim(POSTT("yParola1", ""));
            $yParola2 = trim(POSTT("yParola2", ""));
            if ((count($eParola) > 0) && (count($yParola1) > 0) && (count($yParola2) > 0)) {
                if ($yParola1 != $yParola2) {
                    echo "<script> alert('Yeni Parolalar uyuşmuyor. Lütfen Tekrar Deneyin.'); </script>";
                } else {
                    $kul = $Connection->query('SELECT id, password FROM kullanicilar WHERE id = ' . $_SESSION['user_id'])->fetch();
                    if ($kul) {
                        if ($kul["password"] == $eParola) {
                            $Connection->exec("update kullanicilar set password = '" . $yParola1 . "' where id = " . $kul["id"]);
                            echo
                            " <script> " .
                            " alert('Parolanız başarı ile güncellendi.');" .
                            " window.location='index.php';" .
                            " </script>";
                        } else {
                            echo "<script> alert('Mevcut parola doğrulanamadı. Lütfen Tekrar Deneyin.'); </script>";
                        }
                    }
                }
            }
        }
        ?>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-body" style="text-align: center">
                        <a href="index.php"><img src="resource/img/ag.png" alt=""/></a>
                    </div>
                    <div class="panel-heading">
                        <h3 class="panel-title" style="text-align: center">Parola Güncelle</h3>

                    </div>
                    <div class="panel-body">
                        <form role="form" method="POST" action="sifreDegistir.php">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Mevcut Parola" name="eParola" type="password" value=""/>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Yeni Parola" name="yParola1" type="password" value=""/>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Yeni Parola Tekrar" name="yParola2" type="password" value=""/>
                                </div>
                                <input type="submit" class="btn btn-lg btn-outline btn-success btn-block" value="Değiştir"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include './resource/EndScript.php'; ?> 
</body>
</html>
