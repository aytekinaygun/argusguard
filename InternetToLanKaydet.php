<?php

include './resource/Veritabani.php';

if ($_POST) {
    $id = htmlspecialchars($_POST["id"]);
    $if = htmlspecialchars_decode($_POST["if"]);
    $sGroup = htmlspecialchars_decode($_POST["sGroup"]);
    $sName = htmlspecialchars_decode($_POST["sName"]);
    $dName = htmlspecialchars_decode($_POST["dName"]);
    $service = htmlspecialchars_decode($_POST["service"]);
    $redirectHost = htmlspecialchars_decode($_POST["redirectHost"]);
    $redirectService = htmlspecialchars_decode($_POST["redirectService"]);
    $policy = htmlspecialchars_decode($_POST["policy"]);
    $comment = htmlspecialchars_decode($_POST["comment"]);


    if ($id == 0) {
        $query = $Connection->prepare('INSERT INTO `internettolan`
            (`comment`,`dName`,`if`,`policy`,`service`,`sGroup`,`sName`,`redirectHost`,`redirectService`) VALUES
            (:comment,:dName,:if,:policy,:service,:sGroup,:sName,:redirectHost,:redirectService);             ');
        $query->execute(array('comment' => $comment, 'dName' => $dName, 'if' => $if, 'policy' => $policy, 'service' => $service,
            'sGroup' => $sGroup, 'sName' => $sName, 'redirectHost' => $redirectHost, 'redirectService' => $redirectService));
    } else {
        $query = $Connection->prepare('
            UPDATE `internettolan`
            SET
            `comment` = :comment,
            `dName` = :dName,
            `if` = :if,
            `policy` = :policy,
            `service` = :service,
            `sGroup` = :sGroup,
            `redirectHost` = :redirectHost,
            `redirectService` = :redirectService,
            `sName` = :sName
            WHERE `id` = :id;
            ');
        $query->execute(array('comment' => $comment, 'dName' => $dName, 'if' => $if, 'policy' => $policy, 'service' => $service,
            'sGroup' => $sGroup, 'sName' => $sName, 'id' => $id, 'redirectHost' => $redirectHost, 'redirectService' => $redirectService));
    }
}
$Connection = NULL;
header('Location:InternetToLans.php');
exit();
