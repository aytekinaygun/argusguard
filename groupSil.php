<?php

include './resource/Veritabani.php';

if ($_GET) {
    $id = htmlspecialchars($_GET["id"]);
    if ($id >= 0) {
        $query = $Connection->prepare('delete from hostgroup where id = :id;');
        $query->execute(array('id' => $id));
        $query2 = $Connection->prepare('delete from groupitem where group = :id;');
        $query2->execute(array('id' => $id));
    }
}
$Connection = NULL;
header('Location:groups.php');
exit();
?>