
<ul class="nav nav-pills" >
    <li <?php echo $FiltreGrubuMenu == 1 ? 'class="active"' : ''; ?> ><a href="FiltreGrubu-TemelKurallar.php?id=<?php echo $id; ?>">Temel Kurallar</a></li>
    <li <?php echo $FiltreGrubuMenu == 9 ? 'class="active"' : ''; ?> ><a href="FiltreGrubu-Kullanicilar.php?id=<?php echo $id; ?>">Kullanicilar</a></li>
    <li <?php echo $FiltreGrubuMenu == 2 ? 'class="active"' : ''; ?> ><a href="FiltreGrubu-IzinliSiteler.php?id=<?php echo $id; ?>">İzinli Siteler</a></li>
    <li <?php echo $FiltreGrubuMenu == 3 ? 'class="active"' : ''; ?> ><a href="FiltreGrubu-YasakliSiteler.php?id=<?php echo $id; ?>">Yasaklı Siteler</a></li>
    <li <?php echo $FiltreGrubuMenu == 4 ? 'class="active"' : ''; ?> ><a href="FiltreGrubu-FiltreKategorileri.php?id=<?php echo $id; ?>">Filtre Kategorileri</a></li>
    <li <?php echo $FiltreGrubuMenu == 5 ? 'class="active"' : ''; ?> ><a href="FiltreGrubu-Uzantilar.php?id=<?php echo $id; ?>">Uzantilar</a></li>
    <li <?php echo $FiltreGrubuMenu == 6 ? 'class="active"' : ''; ?> ><a href="FiltreGrubu-MineTipe.php?id=<?php echo $id; ?>">Dosya Tipleri</a></li>

    <ul class="nav navbar-top-links navbar-right" style="float: left !important;">
        <div class="btn-group" style="margin-left: 2px">
            <button type="button" class="btn btn-primary <?php echo $FiltreGrubuMenu == 8 ? '' : 'btn-outline'; ?> dropdown-toggle" data-toggle="dropdown"
                    style="padding: 10px 12px; border: 0 solid transparent;">
                Diğerleri <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">    
                <li><a href="FiltreGrubu-DownloadSiteleri.php?id=<?php echo $id; ?>">Download Siteleri</a></li>
                <li><a href="FiltreGrubu-IcerikKelimeleri.php?id=<?php echo $id; ?>">İçerikte Kelimeler</a></li>
                <li><a href="FiltreGrubu-UrlIzinliKelimeler.php?id=<?php echo $id; ?>">URL'de İzinli Kelimeler</a></li>
                <li><a href="FiltreGrubu-UrlYasakliKelimeler.php?id=<?php echo $id; ?>">URL'de Yasaklı Kelimeler</a></li>
                <li><a href="FiltreGrubu-IcerikteDegisecekKelimeler.php?id=<?php echo $id; ?>">İçerikte Değişecek Kelimeler</a></li>
            </ul>
        </div>
    </ul>
</ul>