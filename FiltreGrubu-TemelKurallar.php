<!DOCTYPE html>
<html lang="tr">
    <head>
        <?php
        include './resource/MetaTitleLink.php';
        include './resource/dosyaYollari.php';
        include './resource/DosyaIslemleri.php';
        if ($_POST) {
            $id = $_POST['id'];
            $GrupAdi = $_POST['GrupAdi'];
            $GrupMod = $_POST['GrupMod'];
            $GrupKel = $_POST['GrupKel'];

            DosyadanBilgiDegistir($DansguardianKlasoru . 'dansguardianf' . $id . '.conf', array('groupname', 'groupmode', 'naughtynesslimit'), array("'" . $GrupAdi . "'", $GrupMod, $GrupKel));

            header('Location:FiltreGruplari.php');
            exit();
        } else
        if ($_GET) {
            $id = $_GET['id'];
            $GrupAdi = GrupAdiGetir($DansguardianKlasoru, $id);
            $GrupMod = DosyadanBilgiOku($DansguardianKlasoru . 'dansguardianf' . $id . '.conf', 'groupmode', -1);
            $GrupKel = DosyadanBilgiOku($DansguardianKlasoru . 'dansguardianf' . $id . '.conf', 'naughtynesslimit', "");
        } else {
            header('Location:FiltreGruplari.php');
            exit();
        }
        ?> 
    </head>
    <body>
    <div id="wrapper">
        <?php
        $mSelect = 53;
        include './resource/NavBar.php';
        ?> 
        <div id="page-wrapper">
            <!--BURADAN SONRA İÇERİK GİRİLEBİLİR-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="panel panel-default" style="margin-bottom: 15px; margin-top: -20px;">
                            <div class="panel-heading">
                                <h3 style="margin-bottom: 0;"><?php echo $GrupAdi; ?></h3>
                            </div>
                            <div class="panel-body" style="padding: 3px;">
                                <?php
                                $FiltreGrubuMenu = 1;
                                include './FiltreGrubuMenu.php';
                                ?>   
                            </div>
                        </div>
                        <form id = "form1" name = "form1" role = "form" action = "FiltreGrubu-TemelKurallar.php" method = "POST" onsubmit = "return IpAdresKontrol(document.form1.address)">
                            <div class = "col-lg-6">
                                <div class="row">
                                    <div  class="panel panel-info">
                                        <div class="panel-heading">
                                            Temel Kurallar
                                        </div> 
                                        <div class="panel-body">
                                            <input name = "id" type = "hidden" value = "<?php echo $id . ''; ?>" />
                                            <div class = "form-group">
                                                <label>Filtre Grubu Adı</label>
                                                <input name = "GrupAdi" class = "form-control" placeholder = "Name" value = "<?php echo str_replace("'", "", $GrupAdi); ?>"/>
                                            </div>
                                            <div class = "form-group">
                                                <label>Temel Filtre Özelliği</label>
                                                <select name = "GrupMod" class = "form-control" >
                                                    <option value="0" <?php echo ($GrupMod == 0 ? ' selected = "selected"' : '') ?>>İnternet yasaklı</option> 
                                                    <option value="1" <?php echo ($GrupMod == 1 ? ' selected = "selected"' : '') ?>>İnternet filtreli</option>
                                                    <option value="2" <?php echo ($GrupMod == 2 ? ' selected = "selected"' : '') ?>>İnternet filtresiz</option>                                              
                                                </select>
                                            </div>
                                            <div class = "form-group">
                                                <label>Ağırlıklı Kelime Eşik Değeri</label>
                                                <input  name="GrupKel" type="number" class = "form-control" min="1" max="500" value = "<?php echo $GrupKel; ?>" />
                                            </div>
                                            <br/>
                                            <button type="submit" style=" float: right; width: 120px" class="btn btn-primary" ><i class="fa fa-save"></i> Kaydet</button>
                                            <br/><br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--BURADAN ÖNCE İÇERİK GİRİLEBİLİR-->
        </div>
    </div>
    <?php include './resource/EndScript.php'; ?> 

</body>

</html>
