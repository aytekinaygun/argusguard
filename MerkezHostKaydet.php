<!DOCTYPE html>
<html lang="tr">
    <head>
        <?php include './resource/MetaTitleLink.php'; ?> 
    </head>
    <body>
    <div id="wrapper">
        <?php
        include './resource/NavBar.php';
        include './resource/Veritabani.php';

        if ($_POST) {
            $id = htmlspecialchars($_POST["id"]);
            $name = htmlspecialchars($_POST["name"]);
            $address = htmlspecialchars($_POST["address"]);
            $netmask = htmlspecialchars($_POST["netmask"]);
            $comment = htmlspecialchars_decode($_POST["comment"]);

            if ($id == 0) {
                if ($Connection->exec(' INSERT INTO host (tur,   name,   address,   netmask,   comment) ' .
                                ' VALUES (  1, "' . $name . '", "' . $address . '", "' . $netmask . '", "' . $comment . '") ')) {
                    $id = $Connection->lastInsertId();
                }
            } else {
                $Connection->exec('UPDATE host SET '
                        . 'name="' . $name . '", '
                        . 'address="' . $address . '", '
                        . 'netmask="' . $netmask . '", '
                        . 'comment="' . $comment . '" '
                        . ' WHERE id=' . $id);
            }
        }
        if ($_GET) {
            $silId = htmlspecialchars($_GET["silId"]);
            if ($silId >= 0) {
                $query = $Connection->exec('delete from host where id = ' . $silId);
            }
        }
        ?> 
        <div id="page-wrapper">
            <br/>
            <!--BURADAN SONRA İÇERİK GİRİLEBİLİR-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <a href="MerkezHosts.php">
                            <button type="button" class="btn btn-outline btn-success"><i class="fa fa-undo"></i> Merkezi Host Listesi</button>
                        </a><br>
                        <h3>Merkezi Host <?php echo ($_POST ? "Kayıt" : "Sil"); ?> İşlemi</h3>
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead><tr><th>Kurum</th><th>Durum</th></tr></thead>
                            <tbody>
                                <?php
                                foreach ($Connection->query("SELECT * FROM uzak_baglanti") as $row) {

                                    $lDurum = "";
                                    try {
                                        $UzakDb = new PDO($row['database'], $row['user_name'], $row['password']);
                                        $UzakDb->exec('SET NAMES UTF8');
                                        if ($_POST) {
                                            $KaSayisi = $UzakDb->query("SELECT count(*) KaSayisi from host WHERE merkezId=" . $id)->fetch();
                                            if ($KaSayisi["KaSayisi"] == 0) {
                                                $UzakDb->exec(' INSERT INTO host (tur,   name,    address,    netmask,    comment,   merkezId ) ' .
                                                        ' VALUES (1, "' . $name . '", "' . $address . '", "' . $netmask . '", "' . $comment . '", ' . $id . ' ) ');
                                            } else {
                                                $UzakDb->exec('UPDATE host SET '
                                                        . 'name="' . $name . '", '
                                                        . 'address="' . $address . '", '
                                                        . 'netmask="' . $netmask . '", '
                                                        . 'comment="' . $comment . '" '
                                                        . 'WHERE merkezId=' . $id);
                                            }
                                        } else {
                                            $UzakDb->exec('DELETE FROM host WHERE merkezId=' . $silId);
                                        }
                                        $lDurum = 'İşlem Başarılı.';
                                        $UzakDb = null;
                                    } catch (PDOException $e) {
                                        $lDurum = 'Hata Oluştu: ' . $e->getMessage();
                                    }

                                    echo
                                    '<tr>' .
                                    '   <td>' . $row['kurum_adi'] . '</td>' .
                                    '   <td>' . $lDurum . '</td>' .
                                    '</tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--BURADAN ÖNCE İÇERİK GİRİLEBİLİR-->
        </div>
    </div>
    <?php
    $Connection = null;
    include './resource/EndScript.php';
    ?> 

</body>

</html>