<?php

include './resource/Veritabani.php';

if ($_POST) {
    $id = htmlspecialchars($_POST["id"]);
    $if = htmlspecialchars_decode($_POST["if"]);
    $sGroup = htmlspecialchars_decode($_POST["sGroup"]);
    $sName = htmlspecialchars_decode($_POST["sName"]);
    $service = htmlspecialchars_decode($_POST["service"]);
    $policy = htmlspecialchars_decode($_POST["policy"]);
    $comment = htmlspecialchars_decode($_POST["comment"]);


    if ($id == 0) {
        $query = $Connection->prepare('INSERT INTO `inputfirewall`
            (`comment`,`if`,`policy`,`service`,`sGroup`,`sName`) VALUES (:comment,:if,:policy,:service,:sGroup,:sName); ');
        $query->execute(array('comment' => $comment, 'if' => $if, 'policy' => $policy, 'service' => $service, 'sGroup' => $sGroup, 'sName' => $sName));
    } else {
        $query = $Connection->prepare('
            UPDATE `inputfirewall`
            SET
            `comment` = :comment,
            `if` = :if,
            `policy` = :policy,
            `service` = :service,
            `sGroup` = :sGroup,
            `sName` = :sName
            WHERE `id` = :id;
            ');
        $query->execute(array('comment' => $comment, 'if' => $if, 'policy' => $policy, 'service' => $service, 'sGroup' => $sGroup, 'sName' => $sName, 'id' => $id));
    }
}
$Connection = NULL;
header('Location:InputFirewalls.php');
exit();
