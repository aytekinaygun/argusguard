<!DOCTYPE html>
<html lang="tr">
    <head>
        <?php include './resource/MetaTitleLink.php'; ?> 
    </head>
    <body>
    <div id="wrapper">
        <?php
        include './resource/NavBar.php';
        include './resource/Veritabani.php';

        if ($_POST) {
            $id = htmlspecialchars($_POST["id"]);
            $name = htmlspecialchars_decode($_POST["name"]);
            $port = htmlspecialchars_decode($_POST["port"]);
            $protokol = htmlspecialchars_decode($_POST["protokol"]);
            $comment = htmlspecialchars_decode($_POST["comment"]);

            if ($id == 0) {
                if ($Connection->exec(' INSERT INTO service ( tur, name, port, protokol, comment) ' .
                                ' VALUES (  1, "' . $name . '", "' . $port . '", "' . $protokol . '", "' . $comment . '") ')) {
                    $id = $Connection->lastInsertId();
                }
            } else {
                $query = $Connection->prepare('UPDATE service SET name=:name, port=:port, protokol=:protokol, comment=:comment WHERE id = :id;');
                $query->execute(array('name' => $name, 'port' => $port, 'protokol' => $protokol, 'comment' => $comment, 'id' => $id));
            }
        }
        if ($_GET) {
            $silId = htmlspecialchars($_GET["silId"]);
            if ($silId >= 0) {
                $query = $Connection->exec('delete from service where id = ' . $silId);
            }
        }
        ?> 
        <div id="page-wrapper">
            <br/>
            <!--BURADAN SONRA İÇERİK GİRİLEBİLİR-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <a href="MerkezServices.php">
                            <button type="button" class="btn btn-outline btn-success"><i class="fa fa-undo"></i> Merkezi Servis Listesi</button>
                        </a><br>
                        <h3>Merkezi Servis <?php echo ($_POST ? "Kayıt" : "Sil"); ?> İşlemi</h3>
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead><tr><th>Kurum</th><th>Durum</th></tr></thead>
                            <tbody>
                                <?php
                                foreach ($Connection->query("SELECT * FROM uzak_baglanti") as $row) {

                                    $lDurum = "";
                                    try {
                                        $UzakDb = new PDO($row['database'], $row['user_name'], $row['password']);
                                        $UzakDb->exec('SET NAMES UTF8');
                                        if ($_POST) {
                                            $KaSayisi = $UzakDb->query("SELECT count(*) KaSayisi from service WHERE merkezId=" . $id)->fetch();
                                            if ($KaSayisi["KaSayisi"] == 0) {
                                                $query2 = $UzakDb->prepare('INSERT INTO service ( tur, name, port, protokol, comment, merkezId) ' .
                                                        ' VALUES ( 1, :name, :port, :protokol, :comment, :merkezId);');
                                                $query2->execute(array('name' => $name, 'port' => $port, 'protokol' => $protokol, 'comment' => $comment, 'merkezId' => $id));
                                            } else {
                                                $query2 = $UzakDb->prepare('UPDATE service SET name=:name, port=:port, protokol=:protokol, comment=:comment WHERE merkezId = :id;');
                                                $query2->execute(array('name' => $name, 'port' => $port, 'protokol' => $protokol, 'comment' => $comment, 'id' => $id));
                                            }
                                        } elseif ($_GET) {
                                            $UzakDb->exec('DELETE FROM service WHERE merkezId=' . $silId);
                                        }
                                        $lDurum = 'İşlem Başarılı.';
                                        $UzakDb = null;
                                    } catch (PDOException $e) {
                                        $lDurum = 'Hata Oluştu: ' . $e->getMessage();
                                    }

                                    echo
                                    '<tr>' .
                                    '   <td>' . $row['kurum_adi'] . '</td>' .
                                    '   <td>' . $lDurum . '</td>' .
                                    '</tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--BURADAN ÖNCE İÇERİK GİRİLEBİLİR-->
        </div>
    </div>
    <?php
    $Connection = null;
    include './resource/EndScript.php';
    ?> 

</body>

</html>