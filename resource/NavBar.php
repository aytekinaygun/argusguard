
<?php
session_start();
if (!($_SESSION['oturum'])) {
    header('Location:giris.php');
    exit();
}

function MenuSec($mSelect, $mid) {
    AnaMenuSec($mSelect, $mid, $mid);
}

function AnaMenuSec($mSelect, $mid1, $mid2) {
    echo (($mSelect >= $mid1 && $mSelect <= $mid2) ? ' class="active" ' : '');
}
?>

<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Menü</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="index.php">
            <img style="margin: 6px;" src="resource/img/ag.png" alt="ARGUS Guard"/>
        </a>        
    </div>

    <a href="cikis.php" class="btn btn-outline btn-danger" style="margin: 8px 25px; float: right">
        <i class="fa fa-sign-out fa-fw"></i> Çıkış </a>    
    <a href="sifreDegistir.php" class="btn btn-link" style="margin: 8px 5px; float: right">
        <i class="fa fa-key fa-fw"></i> Parola Değiştir </a>      

    <div class="navbar-default sidebar"  role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li><a <?php AnaMenuSec($mSelect, 0, 1); ?> href="index.php"><img src="resource/img/d.png" alt="Hostlar"/> Ana Sayfa</a></li>
                <li<?php AnaMenuSec($mSelect, 10, 19); ?>><a href="#"><img src="resource/img/0.png"/> Host<span class="fa arrow"></span></a>
                    <ul class="nav nav-third-level">
                        <li><a <?php MenuSec($mSelect, 11); ?> href="hosts.php">Hostlar</a></li>
                        <li><a <?php MenuSec($mSelect, 12); ?> href="<?php echo $_SESSION['merkez_kullanicisi'] ? "MerkezHosts" : "HostsMerkez"; ?>.php" >Merkezi Hostlar</a></li>
                    </ul>
                </li>
                <li<?php AnaMenuSec($mSelect, 20, 29); ?>><a href="#"><img src="resource/img/1.png"/> Host Grupları<span class="fa arrow"></span></a>
                    <ul class="nav nav-third-level">
                        <li><a <?php MenuSec($mSelect, 21); ?> href="groups.php" >Host Grupları</a></li>
                        <li><a <?php MenuSec($mSelect, 22); ?> href="MerkezGroups.php" >Merkezi Gruplar</a></li>
                    </ul>
                </li>
                <li<?php AnaMenuSec($mSelect, 30, 39); ?>><a href="#"><img src="resource/img/s.png"/> Servisler<span class="fa arrow"></span></a>
                    <ul class="nav nav-third-level">
                        <li><a <?php MenuSec($mSelect, 31); ?> href="services.php" >Servisler</a></li>
                        <li><a <?php MenuSec($mSelect, 32); ?> href="MerkezServices.php" >Merkezi Servisler</a></li>
                    </ul>
                </li>
                <li><a <?php AnaMenuSec($mSelect, 40, 49); ?> href="FirewallKurallariAnaMenu.php"><img src="resource/img/f.png" alt="Firewall Kuralları"/>Firewall Kuralları</a></li>
                <!--<li<?php AnaMenuSec($mSelect, 40, 49); ?>><a href="#"><img src="resource/img/f.png"/> Firewall Kuralları<span class="fa arrow"></span></a>
                    <ul class="nav nav-third-level">
                        <li><a <?php MenuSec($mSelect, 42); ?> href="FirewallKurallariAnaMenu.php" ><img src="resource/img/internet-to-local.png" height="42" width="170"/>aaaaaa</a></li>
                        <li><a <?php MenuSec($mSelect, 41); ?> href="lanToInternets.php" ><img src="resource/img/local-to-internet.png" height="42" width="170"/>Lan To Internet</a></li>
                        <li><a <?php MenuSec($mSelect, 42); ?> href="InternetToLans.php" ><img src="resource/img/internet-to-local.png" height="42" width="170"/>Internet To Lan</a></li>
                        <li><a <?php MenuSec($mSelect, 43); ?> href="InputFirewalls.php" ><img src="resource/img/input-firewall.png" height="42" width="170"/>Input Firewall</a></li>
                    </ul>
                </li>-->
                <li<?php AnaMenuSec($mSelect, 50, 59); ?>><a href="#"><img src="resource/img/w.png"/> Web Filtreleme<span class="fa arrow"></span></a>
                    <ul class="nav nav-third-level">
                        <li><a <?php MenuSec($mSelect, 51); ?> href="KullanicilaraIzinliSiteler.php" >Tüm Kullanıcılara İzinli Siteler</a></li>
                        <li><a <?php MenuSec($mSelect, 52); ?> href="KullanicilaraYasakliSiteler.php" >Tüm Kullanıcılara Yasaklı Siteler</a></li>
                        <li><a <?php MenuSec($mSelect, 53); ?> href="FiltreGruplari.php" >Filtre Grupları</a></li>
                        <li><a <?php AnaMenuSec($mSelect, 2, 9); ?> href="Raporlar.php"><img src="resource/img/r.png" alt="Raporlar"/> Raporlar</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>