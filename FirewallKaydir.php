<?php

include './resource/Veritabani.php';
include './resource/Araclar.php';

$id = GETT("id", 0);
$yon = GETT("yon", 0);
$sayfaId = GETT("sayfa", 0);

echo '-' . $id . '-' . $yon . '-' . $sayfaId;

if ($sayfaId == 1) {
    $tablo = "lantointernet";
    $sayfa = "lanToInternets.php";
} elseif ($sayfaId == 2) {
    $tablo = "internettolan";
    $sayfa = "InternetToLans.php";
} else {
    $tablo = "inputfirewall";
    $sayfa = "InputFirewalls.php";
}

if ($yon > 0) {
    $sqlFn = $yon == 1 ? "max" : "min";
    $sqlSr = $yon == 1 ? "<" : ">";
//    echo 'SELECT coalesce(' . $sqlFn . '(id),0) FROM ' . $tablo . ' where id ' . $sqlSr . ' ' . $id;
    $new = $Connection->query('SELECT coalesce(' . $sqlFn . '(id),0) newid FROM ' . $tablo . ' where id ' . $sqlSr . ' ' . $id)->fetch();
    $newId = $new['newid'];
    if ($newId > 0) {
//        echo 'update ' . $tablo . ' set id = 0 where id =' . $newId;
//        echo 'update ' . $tablo . ' set id = ' . $newId . ' where id =' . $id;
//        echo 'update ' . $tablo . ' set id = ' . $id . ' where id = 0';
        $Connection->exec('update ' . $tablo . ' set id = 0 where id =' . $newId);
        $Connection->exec('update ' . $tablo . ' set id = ' . $newId . ' where id =' . $id);
        $Connection->exec('update ' . $tablo . ' set id = ' . $id . ' where id = 0');
    }
} elseif ($id >= 0) {
    $Connection->exec('delete from lantointernet where id =' . $id);
}

$Connection = NULL;
header('Location:' . $sayfa);
exit();
?>