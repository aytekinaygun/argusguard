<!DOCTYPE html>
<html lang="tr">
    <head>
        <?php
        include './resource/MetaTitleLink.php';
        include './resource/system.php';
        include './resource/servisler.php';
        include './resource/DosyaIslemleri.php';
        include './resource/Araclar.php';
        $graphic_refresh = shell_exec("curl \"http://127.0.0.1:9999/monitorix-cgi/monitorix.cgi?mode=localhost&graph=all&when=1day&color=white\" > /dev/null 2>&1");

        $cmd = GETT("i", 0);
        if ($cmd > 0) {
            $output = shell_exec($servisListesi[($cmd - 1)]["start"]);
            $output2 = $output;
            
            header('Location:index.php');
            exit();
        }
        ?> 
    <script>
        function SaatGetir() {
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else { // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("txtSaat").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "resource/saat.php", true);
            xmlhttp.send();
        }
    </script>
    <style>
        .K{ font-weight: bold; }
        .C{ text-align: center; }
        .P{ 
            padding: 10px 10px 10px 4px; 
            float: right;
            margin: -11px;
            color: #000000;
        }
        .M{
            padding: 10px;
            background: none;
            color: #000000;
            margin-bottom: 10px;
        }
        .BTN{
            float: right;
            width: 120px;
            margin: -7px;
        }
    </style>
</head>
<body>
<div id="wrapper">
    <?php
    $mSelect = 1;
    include './resource/NavBar.php';
    ?> 
    <div id="page-wrapper">
        <!--BURADAN SONRA İÇERİK GİRİLEBİLİR-->
        <br>
        <div class="row">
            <div class="col-lg-6">
                <div  class="panel panel-info">
                    <div class="panel-heading">Genel Bilgiler</div>
                    <div class="panel-body">
                        <div class="alert alert-warning col-lg-12  M">Sistem Saati
                            <div class="alert alert-warning col-lg-8 C P"><div id="txtSaat">00.00.0000 00:00:00</div></div></div>
                        <div class="alert alert-warning col-lg-12  M">Oturum Açan
                            <div class="alert alert-warning col-lg-8 C P"><?php echo $_SESSION['full_name']; ?> </div></div>
                        <div class="alert alert-warning col-lg-12  M">Oturum Açılışı
                            <div class="alert alert-warning col-lg-8 C P"><?php echo $_SESSION['giris_zamani']; ?> </div></div>
                        <div class="alert alert-warning col-lg-12  M">Sistem Versiyon
                            <div class="alert alert-warning col-lg-8 C P"><?php include './resource/versiyon.php'; ?> </div></div>
                    </div>
                </div>
                <div  class="panel panel-info">
                    <div class="panel-heading">Sistem Araçları</div>
                    <div class="panel-body">
                        <?php
                        $komutId = 0;
                        foreach ($servisListesi as $srv) {
                            $sonuc = shell_exec($srv["durum"]);
                            echo
                            ' <div class = "alert alert-warning col-lg-12  M">' . $srv["adi"] .
                            ' <div class = "alert alert-warning col-lg-6 C P">' . ((trim($sonuc) == "T") ? "Çalışıyor" : "Durdu") .
                            ' <a href = "index.php?i=' . ($komutId + 1) . '" class = "btn btn-outline btn-primary BTN">' .
                            ((trim($sonuc) == "T") ? "Yeniden Başlat" : "Başlat") . '</a></div></div> ';
                            $komutId++;
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div  class="panel panel-info">
                    <div class="panel-heading">Sistem Yük Bilgisi</div>
                    <div class="panel-body" style="padding: 2px;text-align: center;">
                        <img id="graphic1" style="max-width: 100%;" src="http://192.168.0.29:9999/monitorix/imgs/system1z.1day.png"  alt=""/>
                    </div>
                </div>   
                <div class="panel panel-info">
                    <div class="panel-heading">Bellek Kullanımı</div>
                    <div class="panel-body" style="padding: 5px;text-align: center;">
                        <img id="graphic2" style="max-width: 100%;" src="http://127.0.0.1:9999/monitorix/imgs/system3z.1day.png" alt=""/>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">Disk Kullanımı</div>
                    <div class="panel-body" style="padding: 5px;text-align: center;">
                        <img id="graphic3" style="max-width: 100%;" src="http://127.0.0.1:9999/monitorix/imgs/fs01z.1day.png" alt=""/>
                    </div>
                </div>


                <?php
                foreach (DosyaninSatirListesi("./resource/info_if") as $row) {
                    $Adim = explode(' ', $row["v"]);
                    echo
                    '<div class = "panel panel-info"> ' .
                    '<div class = "panel-heading">' . $row["v"] . '</div> ' .
                    '<div class = "panel-body" style = "padding: 5px;text-align: center;"> ' .
                    '<img id = "' . $Adim[0] . '" style = "max-width: 100%;" src = "' . $Adim[0] . '.png" alt = ""/> ' .
                    '</div> ' .
                    '</div> ';
                }
                ?>
            </div>
        </div>


        <!--BURADAN ÖNCE İÇERİK GİRİLEBİLİR-->
    </div>
</div>
<?php include './resource/EndScript.php'; ?> 
<script>
    SaatGetir();
    setInterval(function () {
        SaatGetir();
    }, 1000);
    function refresh(node)
    {
        var times = 1000 * 60; // 60 saniye;

        (function startRefresh()
        {
            var address;
            if (node.src.indexOf('?') > -1)
                address = node.src.split('?')[0];
            else
                address = node.src;
            node.src = address + "?time=" + new Date().getTime();

            setTimeout(startRefresh, times);
        })();

    }

    window.onload = function ()
    {
        refresh(document.getElementById('graphic1'));
        refresh(document.getElementById('graphic2'));
        refresh(document.getElementById('graphic3'));
        refresh(document.getElementById('eth0'));
<?php
foreach (DosyaninSatirListesi("./resource/info_if") as $row) {
    $Adim = explode(' ', $row["v"]);
    echo "refresh(document.getElementById('" . $Adim[0] . "'));";
}
?>
    }
</script>
</body>

</html>
