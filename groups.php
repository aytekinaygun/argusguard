<!DOCTYPE html>
<html lang="tr">
    <head>
        <?php
        include './resource/MetaTitleLink.php';
        include './resource/Veritabani.php';
        ?> 
    </head>
    <body>
    <div id="wrapper">
        <?php
        $mSelect = 21;
        include './resource/NavBar.php';
        ?> 
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Host Grubu Kayıtları
                        <a href="group.php">
                            <button type="button" style="float: right" class="btn btn-outline btn-info"><i class="fa fa-plus"></i> Group Ekle</button>
                        </a>
                    </h1>
                </div>
            </div>
            <!--BURADAN SONRA İÇERİK GİRİLEBİLİR-->

            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>Grup Adı</th>
                                    <th>Açıklama</th>
                                    <th>Üye Hostlar</th>
                                    <th style="width: 30px"> </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($Connection->query("  
                                    SELECT g.*, (SELECT group_concat(CONCAT(' ', h.name)) 
                                                   FROM host h
                                                  where h.id in (select i.host from groupitem i where i.group = g.id )) hostlar
                                       FROM hostgroup g
                                      WHERE g.tur = 2
                                   ORDER BY g.name
                                    ") as $row) {
                                    echo '
                                <tr class="odd gradeX">
                                    <td>' . $row['name'] . '</td>
                                    <td>' . $row['comment'] . '</td>
                                    <td>' . $row['hostlar'] . '</td>
                                    <td>
                                        <a href="group.php?id=' . $row['id'] . '">
                                        <button type="button" class="btn btn-outline btn-primary btn-xs"><i class="fa fa-edit"></i></button>
                                        </a>
                                    </td>
                                </tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--BURADAN ÖNCE İÇERİK GİRİLEBİLİR-->
        </div>
    </div>
    <?php include './resource/EndScript.php'; ?>  
    <script>
        $(document).ready(function () {
            $('#dataTables-example').dataTable();
        });
    </script>
</body>

</html>
