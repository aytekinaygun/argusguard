<?php

include './resource/Veritabani.php';

if ($_GET) {
    $id = htmlspecialchars($_GET["id"]);
    if ($id >= 0) {
        $query = $Connection->prepare('delete from host where id = :id;');
        $query->execute(array('id' => $id));
    }
}
header('Location:hosts.php');
exit();
?>