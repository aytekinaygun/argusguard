<!DOCTYPE html>
<html lang="tr">
    <head>
        <?php
        include './resource/MetaTitleLink.php';
        include './resource/dosyaYollari.php';
        include './resource/DosyaIslemleri.php';
        include './resource/Araclar.php';

        $id = GETT('id', 0);
        if ($id == 0) {
            $id = POSTT('id', 0);
        }
        if ($id == 0) {
            header('Location:FiltreGruplari.php');
            exit();
        }
        if (empty($VeriOnEk)) {
            $VeriOnEk = '';
        }
        if (empty($Alan1Adi)) {
            $Alan1Adi = 'Adı';
        }
        if (empty($Alan2Adi)) {
            $Alan2Adi = 'Açıklama';
        }
        if (empty($VeriArkaEk)) {
            $VeriArkaEk = '';
        }
        if (empty($Ayrac)) {
            $Ayrac = '#';
        }

        $tur = POSTT('tur', 0);
        $name = POSTT_ON_ARKA('name', '', $VeriOnEk, $VeriArkaEk);
        $aktif = POSTT('aktif', false);
        $oldName = POSTT_ON_ARKA('oldName', '', $VeriOnEk, $VeriArkaEk);
        $silName = POSTT_ON_ARKA('silName', '', $VeriOnEk, $VeriArkaEk);
        $aciklama = POSTT_ON_ARKA('aciklama', '', $VeriOnEk, $VeriArkaEk);

        $GrupAdi = GrupAdiGetir($DansguardianKlasoru, $id);
        $liste1DosyaAdi = $DansguardianKlasoru . 'lists/filtergroups' . $id . '/' . $liste1DosyaAdiSadece;
        $liste2DosyaAdi = $DansguardianKlasoru . 'lists/filtergroups' . $id . '/' . $liste2DosyaAdiSadece;
        $listeDosyaAdi = (($tur == 1) ? $liste1DosyaAdi : $liste2DosyaAdi);


        if (strlen($silName) > 0) {
            DosyadanSatirSil1($listeDosyaAdi, $silName, $Ayrac);
        } elseif (strlen($oldName) > 0) {
            DosyadanSatirDegistir1($listeDosyaAdi, $oldName, $name, $aciklama, $aktif, $Ayrac);
        } elseif (strlen($name) > 0) {
            DosyayaSatirEkle1($listeDosyaAdi, $name, $aciklama, $aktif, $Ayrac);
        }
        ?> 
    </head>
    <body>
    <div id="wrapper">
        <?php
        $mSelect = 53;
        include './resource/NavBar.php';
        ?> 
        <div id="page-wrapper">
            <!--BURADAN SONRA İÇERİK GİRİLEBİLİR-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="panel panel-default" style="margin-bottom: 15px; margin-top: -20px;">
                            <div class="panel-heading">
                                <h3 style="margin-bottom: 0;"><?php echo $GrupAdi; ?></h3>
                            </div>
                            <div class="panel-body" style="padding: 3px;">
                                <?php
                                include './FiltreGrubuMenu.php';
                                ?>   
                            </div>
                        </div>

                        <div class="col-lg-<?php echo (strlen($liste2DosyaAdiSadece) > 0) ? '6' : '12'; ?>">
                            <div class="row">
                                <div  class="panel panel-<?php echo $tema; ?>">
                                    <div class="panel-heading">
                                        <?php echo $liste1ListeAdi; ?>
                                        <button type="button" class="btn btn-outline btn-<?php echo $tema; ?>" onclick="FormuAc('', '', '', 'true', 1);" 
                                                style="margin: -7px 0px; float: right; width: 142px;" >
                                            <i class="fa fa-plus"></i> Yeni Kayıt
                                        </button>
                                    </div>
                                    <div class="panel-body">
                                        <table class="table table-striped table-bordered table-hover" id="grdListe1" >
                                            <thead style="display: none">
                                                <tr>
                                                    <th></th>
                                                    <?php echo ($AciklamaVar ? '<th></th>' : ''); ?>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach (DosyaninSatirListesi1($liste1DosyaAdi, $Ayrac) as $row) {
                                                    $lAktif = StrTemizle($row["a"]);
                                                    $lVeri = StrTemizle($row["v"]);
                                                    $lAciklama = StrTemizle($row["ac"]);
                                                    echo
                                                    '<tr ' . ($lAktif ? '' : 'style="text-decoration:line-through;"') . '> ' .
                                                    '   <td>' . str_replace(' ', '&nbsp;', $row["v"]) .
                                                    ($AciklamaVar ? '   </td><td style="width: 50%">' . $lAciklama : "" ) .
                                                    '   </td>' .
                                                    '   <td style="width: 56px">' .
                                                    '       <button onclick="SilFormuAc(\'' . $lVeri . '\',1);" type="button" ' .
                                                    '             style="float: right; margin-left: 5px;"  class="btn btn-outline btn-danger btn-xs"><i class="fa fa-times"></i></button> ' .
                                                    '       <button onclick="FormuAc(\'' . $lVeri . '\',\'' . $lVeri . '\',\'' . $lAciklama . '\',' . ($lAktif ? 'true' : 'false') . ',1);" type="button" ' .
                                                    '            style="float: right; margin-left: 5px;"   class="btn btn-outline btn-primary btn-xs"><i class="fa fa-edit"></i></button> ' .
                                                    '   </td> ' .
                                                    '</tr>';
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6" <?php echo (strlen($liste2ListeAdi) > 0) ? '' : 'style="display: none;"'; ?> >
                            <div  class="panel panel-<?php echo $tema; ?>">
                                <div class="panel-heading">
                                    <?php echo $liste2ListeAdi; ?>
                                    <button type="button" class="btn btn-outline btn-<?php echo $tema; ?>" onclick="FormuAc('', '', '', 'true', 2);" 
                                            style="margin: -7px 0px; float: right; width: 142px;" >
                                        <i class="fa fa-plus"></i> Yeni Kayıt
                                    </button>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-striped table-bordered table-hover" id="grdListe2" >
                                        <thead style="display: none">
                                            <tr>
                                                <th></th>
                                                <?php echo ($AciklamaVar ? '<th></th>' : ''); ?>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (strlen($liste2ListeAdi) > 0) {
                                                foreach (DosyaninSatirListesi1($liste2DosyaAdi, $Ayrac) as $row) {
                                                    $lAktif = StrTemizle($row["a"]);
                                                    $lVeri = StrTemizle($row["v"]);
                                                    $lAciklama = StrTemizle($row["ac"]);
                                                    echo
                                                    '<tr ' . ($lAktif ? '' : 'style="text-decoration:line-through;"') . '> ' .
                                                    '   <td>' . str_replace(' ', '&nbsp;', $row["v"]) .
                                                    ($AciklamaVar ? '   </td><td style="width: 50%">' . $lAciklama : "" ) .
                                                    '   </td>' .
                                                    '   <td style="width: 56px">' .
                                                    '       <button onclick="SilFormuAc(\'' . $lVeri . '\',2);" type="button" ' .
                                                    '             style="float: right; margin-left: 5px;"  class="btn btn-outline btn-danger btn-xs"><i class="fa fa-times"></i></button> ' .
                                                    '       <button onclick="FormuAc(\'' . $lVeri . '\',\'' . $lVeri . '\',\'' . $lAciklama . '\',' . ($lAktif ? 'true' : 'false') . ',2);" type="button" ' .
                                                    '            style="float: right; margin-left: 5px;"   class="btn btn-outline btn-primary btn-xs"><i class="fa fa-edit"></i></button> ' .
                                                    '   </td> ' .
                                                    '</tr>';
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="frmDuzenle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Bilgi Düzenleme</h4>
                        </div> 
                        <form id = "f1" name = "f1" role = "form" action = "<?php echo $SayfaUrl; ?>" method = "POST" >
                            <div class="modal-body">
                                <div class = "row">
                                    <div class = "col-lg-12">
                                        <div class = "form-group">
                                            <label><?php echo $Alan1Adi; ?></label>
                                            <input id="xid" name = "id" type = "hidden" value="<?php echo $id . ""; ?>" />
                                            <input id="xtur" name = "tur" type = "hidden" />
                                            <input id="xname" name = "name" class = "form-control" />
                                            <input id="xoldName" name = "oldName" type = "hidden"  />
                                            <?php echo ($AciklamaVar ? '<br><label>' . $Alan2Adi . '</label>' : ''); ?>
                                            <input id="xaciklama" name = "aciklama" class = "form-control" <?php echo (!$AciklamaVar ? 'style="display: none;"' : ''); ?> />
                                        </div>
                                        <div class = "form-group">
                                            <label><input id="xaktif" name="aktif" type="checkbox" /> Aktif</label>
                                        </div>
                                    </div>    
                                </div>    
                            </div>
                            <div class="modal-footer">
                                <button type="submit"  class="btn btn-primary" ><i class="fa fa-save"></i> Kaydet</button>
                                <button type = "button" class = "btn btn-default" data-dismiss = "modal">İptal</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <button id="btnFrmDuzenle" type="button"  data-toggle="modal" data-target="#frmDuzenle" style="display: none;" ></button>
            <script>
                function FormuAc(aOldName, aValue, aAciklama, aAktif, aTur) {
                    $('#xtur').val(aTur);
                    $('#xname').val(aValue);
                    $('#xoldName').val(aOldName);
                    $('#xaciklama').val(aAciklama);
                    $('#xaktif').prop("checked", aAktif);
                    $('#btnFrmDuzenle').click();
                }
            </script>

            <div class="modal fade" id="frmSil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Dikkat</h4>
                        </div>
                        <form id = "f2" name = "f2" role = "form" action = "<?php echo $SayfaUrl; ?>" method = "POST" >
                            <div class="modal-body">
                                <input id="zId" name = "id" type = "hidden" value="<?php echo $id . ""; ?>" />
                                <input id="zTur" name = "tur" type = "hidden"  />
                                Kaydı [<span id="zName"></span>] silmek istediğinizden emin misiniz?
                                <input id="zSilName" name = "silName" type = "hidden"  />
                            </div>
                            <div class="modal-footer">
                                <button type="submit"  class="btn btn-danger" ><i class="fa fa-times"></i> Sil</button>
                                <button type="button" class = "btn btn-default" data-dismiss = "modal">İptal</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <button id="btnFrmSil" type="button"  data-toggle="modal" data-target="#frmSil" style="display: none;" ></button>
            <script>
                function SilFormuAc(aValue, aTur) {
                    $('#zName').html(aValue);
                    $('#zSilName').val(aValue);
                    $('#zTur').val(aTur);
                    $('#btnFrmSil').click();
                }
            </script>

        </div>
    </div>
    <?php include './resource/EndScript.php'; ?> 
    <script>
        $(document).ready(function () {
            $('#grdListe1').dataTable();
            $('#grdListe2').dataTable();
        });
    </script>
</body>

</html>
