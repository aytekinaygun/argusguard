<?php

include './resource/dosyaYollari.php';
include './resource/DosyaIslemleri.php';

function kopyala($dk, $GrupSayisi, $Dosya) {
    copy($dk . "lists/filtergroups-default/" . $Dosya, $dk . "lists/filtergroups" . $GrupSayisi . "/" . $Dosya);
}

function degistir($dk, $GrupSayisi, $Dosya) {
    $dosyaIci = file_get_contents($dk . "lists/filtergroups-default/" . $Dosya);
    $dosyaIci2 = str_replace("/filtergroups/", "/filtergroups" . $GrupSayisi . "/", $dosyaIci);
    file_put_contents($dk . "lists/filtergroups" . $GrupSayisi . "/" . $Dosya, $dosyaIci2);
}

$GrupSayisi = DosyadanBilgiOku($DansguardianKlasoru . 'dansguardian.conf', 'filtergroups', 0) + 1;
DosyadanBilgiDegistir($DansguardianKlasoru . 'dansguardian.conf', array('filtergroups'), array($GrupSayisi));


echo $GrupSayisi;
$dansguardianconf = file_get_contents($DansguardianKlasoru . 'dansguardian-default.conf');
$dansguardianconf2 = str_replace("/filtergroupsX/", "/filtergroups" . $GrupSayisi . "/", $dansguardianconf);
file_put_contents($DansguardianKlasoru . 'dansguardianf' . $GrupSayisi . '.conf', $dansguardianconf2);


$KlasorAdi = $DansguardianKlasoru . "lists/filtergroups" . $GrupSayisi;
mkdir($KlasorAdi);

kopyala($DansguardianKlasoru, $GrupSayisi, "bannedextensionlist");
kopyala($DansguardianKlasoru, $GrupSayisi, "bannedmimetypelist");
kopyala($DansguardianKlasoru, $GrupSayisi, "bannedphraselist");
kopyala($DansguardianKlasoru, $GrupSayisi, "bannedregexpheaderlist");
kopyala($DansguardianKlasoru, $GrupSayisi, "bannedregexpurllist");
degistir($DansguardianKlasoru, $GrupSayisi, "bannedsitelist");
kopyala($DansguardianKlasoru, $GrupSayisi, "bannedsitelist-fg");
kopyala($DansguardianKlasoru, $GrupSayisi, "bannedsitelist-kategori");
degistir($DansguardianKlasoru, $GrupSayisi, "bannedurllist");
kopyala($DansguardianKlasoru, $GrupSayisi, "bannedurllist-fg");
kopyala($DansguardianKlasoru, $GrupSayisi, "bannedurllist-kategori");
kopyala($DansguardianKlasoru, $GrupSayisi, "contentregexplist");
kopyala($DansguardianKlasoru, $GrupSayisi, "exceptionextensionlist");
kopyala($DansguardianKlasoru, $GrupSayisi, "exceptionfilesitelist");
kopyala($DansguardianKlasoru, $GrupSayisi, "exceptionfileurllist");
kopyala($DansguardianKlasoru, $GrupSayisi, "exceptionmimetypelist");
kopyala($DansguardianKlasoru, $GrupSayisi, "exceptionphraselist");
kopyala($DansguardianKlasoru, $GrupSayisi, "exceptionregexpurllist");
degistir($DansguardianKlasoru, $GrupSayisi, "exceptionsitelist");
kopyala($DansguardianKlasoru, $GrupSayisi, "exceptionsitelist-fg");
kopyala($DansguardianKlasoru, $GrupSayisi, "exceptionsitelist-kategori");
degistir($DansguardianKlasoru, $GrupSayisi, "exceptionurllist");
kopyala($DansguardianKlasoru, $GrupSayisi, "exceptionurllist-fg");
kopyala($DansguardianKlasoru, $GrupSayisi, "exceptionurllist-kategori");
kopyala($DansguardianKlasoru, $GrupSayisi, "greysitelist");
kopyala($DansguardianKlasoru, $GrupSayisi, "greyurllist");
kopyala($DansguardianKlasoru, $GrupSayisi, "headerregexplist");
kopyala($DansguardianKlasoru, $GrupSayisi, "pics");
kopyala($DansguardianKlasoru, $GrupSayisi, "urlregexplist");
kopyala($DansguardianKlasoru, $GrupSayisi, "weightedphraselist");


header('Location:FiltreGruplari.php');
exit();
