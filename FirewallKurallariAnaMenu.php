<!DOCTYPE html>
<html lang="tr">
    <head>
        <?php
        include './resource/MetaTitleLink.php';
        include './resource/Veritabani.php';
        ?> 
    </head>
    <body>
    <div id="wrapper">
        <?php
        $mSelect = 40;
        include './resource/NavBar.php';
        ?> 
        <div id="page-wrapper">
            <div>
                <div class="row">                    
                        <h1>
                            <a href="lanToInternets.php">     
                                <font size="6"><u>Lokal Ağınızdan İnternete Giden Trafik Kuralları</u></font><br>
                                <img style="float: left"  src="resource/img/local-to-internet.png"  height="130" width="450"/>  
                                <div class="small">
                                    Lokal ağınızdan internet yönüne giden trafiğin kurallarını oluşturacağınız bölüm. Burada oluşturacağınız kurallar içerik filtrelemeye takılmaz. Lokal ağınızdaki herhangi bir IP yada kullanıcıya, internet bulutunda güvendiğiniz herhangi bir sisteme direk erişim izni vermek için kullanabilirsiniz.
                                </div>
                            </a>
                        </h1>
                    <br>
                        <hr>
                        
                        <h1>
                            <a href="InternetToLans.php">     
                                <font size="6"><u>İnternetten Gelen Trafiği Local Ağa İlgili Servise Yönlendirme</u></font><br>
                                <img style="float: left"  src="resource/img/internet-to-local.png"  height="130" width="450"/>                                                                
                                <div class="small">
                                    Lokal ağınızda hizmet verdiğiniz bir servise internet bulutundan bağlanabilmek için kural oluşturabileceğiniz bölüm. Port yönlendirme için kullanabilirsiniz.
                                </div>
                            </a>                            
                        </h1>
                        <br><br><br>
                        <hr>
                        
                        <h1>
                            <a href="InputFirewalls.php">     
                                <font size="6"><u>Güvenlik Duvarının Kendisine Gelen Trafik</u></font><br>
                                <img style="float: left"  src="resource/img/input-firewall.png"  height="130" width="450"/>                                                                
                                <div class="small">
                                    Güvenlik duvarının kendisine; internet bulutundan yada lokal ağdan gelen trafik için kural oluşturabileceğiniz bölüm. Güvenlik duvarında çalışan bir servise bağlanabilmek için, önce bu bölümde izin kuralı oluşturmak gerekir.
                                </div>
                            </a>
                        </h1>    
                        <br><br>
                        <hr>
                </div>
            </div>
        </div>
    </div>
    <?php include './resource/EndScript.php'; ?>  
    <script>
        $(document).ready(function () {
            $('#dataTables-example').dataTable();
        });
    </script>
</body>

</html>
