<!DOCTYPE html>
<html lang="tr">
    <head>
        <?php include './resource/MetaTitleLink.php'; ?> 
    </head>
    <body>
    <div id="wrapper">
        <?php
        $mSelect = 42;
        include './resource/NavBar.php';
        include './resource/Veritabani.php';
        include './resource/dosyaYollari.php';

        $id = 0;
        $if = "";
        $sGroup = "";
        $sName = "";
        $sNameAdi = "";
        $dName = "";
        $dNameAdi = "";
        $service = "";
        $serviceAdi = "";
        $redirectHost = "";
        $redirectHostAdi = "";
        $redirectService = "";
        $redirectServiceAdi = "";
        $policy = "";
        $comment = "";
        if ($_GET) {
            $id = $_GET['id'];

            $row = $Connection->query('       
         SELECT il.*,
                CASE WHEN il.sGroup = 1 THEN "Group" else "Host" end tur1,
                CASE WHEN il.sGroup = 1 THEN
                        (SELECT g.name FROM hostgroup g WHERE g.id = il.sName)
                ELSE
                        (SELECT CONCAT(h.name," (",h.address,")") FROM host h WHERE h.id = il.sName)
                END sNameAdi,
                (SELECT CONCAT(h.name," (",h.address,")") FROM host h WHERE h.id = il.dName) dNameAdi,
                (SELECT CONCAT(h.name,\' - \' ,h.port,\' - \' ,h.protokol) FROM service h WHERE h.id = il.service) serviceAdi,
                (SELECT CONCAT(h.name," (",h.address,")") FROM host h WHERE h.id = il.redirectHost) redirectHostAdi,
                (SELECT CONCAT(h.name,\' - \' ,h.port,\' - \' ,h.protokol) FROM service h WHERE h.id = il.redirectService) redirectServiceAdi
           FROM internettolan il 
          WHERE il.id = ' . $id)->fetch();
            if ($row) {
                $id = $row["id"];
                $if = $row["if"];
                $sGroup = $row["sGroup"];
                $sName = $row["sName"];
                $sNameAdi = $row["sNameAdi"];
                $dName = $row["dName"];
                $dNameAdi = $row["dNameAdi"];
                $service = $row["service"];
                $serviceAdi = $row["serviceAdi"];
                $redirectHost = $row["redirectHost"];
                $redirectHostAdi = $row["redirectHostAdi"];
                $redirectService = $row["redirectService"];
                $redirectServiceAdi = $row["redirectServiceAdi"];
                $policy = $row["policy"];
                $comment = $row["comment"];
            } else {
                header('Location:InternetToLans.php');
                exit();
            }
        }
        ?> 
        <div id="page-wrapper">
            <br/>
            <!--BURADAN SONRA İÇERİK GİRİLEBİLİR-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Kural Düzenle
                            <div style="text-align: right; position: static; float: right">
                                <?php
                                if ($id > 0)
                                    echo '<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#myModal">Kaydı Sil</button>';
                                ?>
                            </div>
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Dikkat</h4>
                                        </div>
                                        <div class="modal-body">
                                            Kaydını silmek istediğinizden emin misiniz?
                                        </div>
                                        <div class="modal-footer">
                                            <a href = "InternetToLanSil.php?id=<?php echo $id; ?>"><button type = "button" class = "btn btn-danger" >Sil</button></a>
                                            <button type = "button" class = "btn btn-default" data-dismiss = "modal">Hayır</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class = "panel-body">
                            <div class = "row">
                                <form id = "form1" name = "form1" role = "form" action = "InternetToLanKaydet.php" method = "POST" onsubmit = "return AciklamaVarMi();">

                                    <input name = "id" type = "hidden" value = "<?php echo $id . ''; ?>" />
                                    <div class = "col-lg-6">

                                        <div class = "form-group">
                                            <label>If</label>
                                            <select name = "if" class = "form-control" >
                                                <?php
                                               foreach ($Connection->query("SELECT CONCAT(x.if,' - ' , coalesce(x.name,'')) iff, x.if FROM interfaces x") as $row) {
                                                    echo '<option' . ($if == $row["if"] ? ' selected = "selected"' : '') . ' value="' . $row["if"] . '" >' . $row["iff"] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>                                          

                                        <input id="sGroup" name = "sGroup" type = "hidden" value = "<?php echo $sGroup . ''; ?>" />
                                        <input id="sName" name = "sName" type = "hidden" value = "<?php echo $sName . ''; ?>" />
                                        <div class = "form-group">
                                            <label>Kaynak</label>
                                            <div class="form-group input-group">
                                                <input id="sNameAdi" name="sNameAdi" type="text" class="form-control" value = "<?php echo $sNameAdi ?>" disabled>
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#HostList" 
                                                                onclick="ListAc('#sGroup', '#sName', '#sNameAdi', true);"><i class="fa fa-search"></i> Host</button>
                                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#GroupList"
                                                                onclick="ListAc('#sGroup', '#sName', '#sNameAdi', true);"><i class="fa fa-search"></i> Group</button>
                                                    </span>
                                            </div>          
                                        </div>                                   

                                        <input id="dName" name = "dName" type = "hidden" value = "<?php echo $dName . ''; ?>" />
                                        <div class = "form-group">
                                            <label>Hedef</label>
                                            <div class="form-group input-group">
                                                <input id="dNameAdi" name="dNameAdi" type="text" class="form-control" value = "<?php echo $dNameAdi ?>" disabled>
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#HostList" 
                                                                onclick="ListAc('#dGroup', '#dName', '#dNameAdi', true);"><i class="fa fa-search"></i> Host</button>
                                                    </span>
                                            </div>
                                        </div>     

                                        <input id="service" name = "service" type = "hidden" value = "<?php echo $service . ''; ?>" />
                                        <div class = "form-group">
                                            <label>Servis</label>
                                            <div class="form-group input-group">
                                                <input id="serviceAdi" name="serviceAdi" type="text" class="form-control" value = "<?php echo $serviceAdi ?>" disabled>
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#ServiceList" 
                                                                onclick="ListAc('', '#service', '#serviceAdi', true);"><i class="fa fa-search"></i> Service</button>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>                                    
                                    <div class = "col-lg-6">   

                                        <input id="redirectHost" name = "redirectHost" type = "hidden" value = "<?php echo $redirectHost . ''; ?>" />
                                        <div class = "form-group">
                                            <label>Yönlendirilen Host</label>
                                            <div class="form-group input-group">
                                                <input id="redirectHostAdi" name="redirectHostAdi" type="text" class="form-control" value = "<?php echo $redirectHostAdi ?>" disabled>
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#HostList" 
                                                                onclick="ListAc('', '#redirectHost', '#redirectHostAdi', false);"><i class="fa fa-search"></i> Host</button>
                                                    </span>
                                            </div>
                                        </div>

                                        <input id="redirectService" name = "redirectService" type = "hidden" value = "<?php echo $redirectService . ''; ?>" />
                                        <div class = "form-group">
                                            <label>Yönlendirilen Servis</label>
                                            <div class="form-group input-group">
                                                <input id="redirectServiceAdi" name="redirectServiceAdi" type="text" class="form-control" value = "<?php echo $redirectServiceAdi ?>" disabled>
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#ServiceList" 
                                                                onclick="ListAc('', '#redirectService', '#redirectServiceAdi', false);"><i class="fa fa-search"></i> Service</button>
                                                    </span>
                                            </div>
                                        </div>

                                        <div class = "form-group">
                                            <label>Kural</label>
                                            <select name = "policy" class = "form-control" >
                                                <option <?php echo $policy == 'ACCEPT' ? ' selected = "selected"' : ''; ?> >ACCEPT</option>
                                                <option <?php echo $policy == 'DROP' ? ' selected = "selected"' : ''; ?> >DROP</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Açıklama</label>
                                            <input id="comment" name="comment" class="form-control" placeholder="Comment" value="<?php echo $comment; ?>">
                                        </div>
                                    </div>
                                    <br/>
                                    <button type="submit" style=" float: right; margin-right: 15px; width: 120px" class="btn btn-primary" ><i class="fa fa-save"></i> Kaydet</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="HostList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button id="HostKapat" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Host Listesi</h4>
                        </div>
                        <div class="modal-body">
                            <table class="table table-striped table-bordered table-hover" id="grdHostList">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Host Adı</th>
                                        <th>IP</th>
                                        <th>Netmask</th>
                                        <th>Açıklama</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($Connection->query('select id, name, address, comment, netmask from host') as $row) {
                                        echo '
                                            <tr id="htr' . $row['id'] . '" style="cursor: pointer" onclick="SecimYap(' . $row['id'] . ',\'' . $row['name'] . ' (' . $row['address'] . ')\',0);">
                                                <td>' . $row['id'] . '</td>
                                                <td>' . $row['name'] . '</td>
                                                <td>' . $row['address'] . '</td>
                                                <td>' . $row['netmask'] . '</td>
                                                <td>' . $row['comment'] . '</td>
                                            </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="GroupList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button id="GroupKapat" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Group Listesi</h4>
                        </div>
                        <div class="modal-body">
                            <table class="table table-striped table-bordered table-hover" id="grdGroupList">
                                <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Grup Adı</th>
                                        <th>Açıklama</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($Connection->query('select id, name, comment from hostgroup') as $row) {
                                        echo '
                                            <tr style="cursor: pointer" onclick="SecimYap(' . $row['id'] . ',\'' . $row['name'] . '\',1);">
                                                <td>' . $row['id'] . '</td>
                                                <td>' . $row['name'] . '</td>
                                                <td>' . $row['comment'] . '</td>
                                            </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="ServiceList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button id="ServiceKapat" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Servis Listesi</h4>
                        </div>
                        <div class="modal-body">
                            <table class="table table-striped table-bordered table-hover" id="grdServiceList">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Servis Adı</th>
                                        <th>Port</th>
                                        <th>Protokol</th>
                                        <th>Açıklama</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($Connection->query('select id, name, port, protokol, comment from service') as $row) {
                                        echo '
                                            <tr id="str' . $row['id'] . '" style="cursor: pointer" onclick="SecimYap(' . $row['id'] . ',\'' . $row['name'] . ' - ' . $row['port'] . ' - ' . $row['protokol'] . '\',-1);">
                                                <td>' . $row['id'] . '</td>
                                                <td>' . $row['name'] . '</td>
                                                <td>' . $row['port'] . '</td>
                                                <td>' . $row['protokol'] . '</td>
                                                <td>' . $row['comment'] . '</td>
                                            </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--BURADAN ÖNCE İÇERİK GİRİLEBİLİR-->
        </div>
    </div>
    <?php include './resource/EndScript.php'; ?>  
    <script>
        $(document).ready(function () {
            $('#grdHostList').dataTable();
            $('#grdGroupList').dataTable();
            $('#grdServiceList').dataTable();
        });

        lGroup = '';
        lName = '';
        lNameAdi = '';
        function ListAc(aGroup, aName, aNameAdi, aId1iGoster) {
            $("#str1").css("display", aId1iGoster ? "" : "none");
            $("#htr1").css("display", aId1iGoster ? "" : "none");
            lGroup = aGroup;
            lName = aName;
            lNameAdi = aNameAdi;
        }
        function SecimYap(aId, aName, aGroup) {
            if (lGroup.length > 1)
                $(lGroup).val(aGroup);
            $(lName).val(aId);
            $(lNameAdi).val(aName);
            $('#HostKapat').click();
            $('#GroupKapat').click();
            $('#ServiceKapat').click();
        }
    </script>
    <script>
        function AciklamaVarMi() {
            var lReturn = $('#comment').val().length > 0;
            if (!lReturn)
                alert("Açıklama alanını boş geçemezsiniz.");
            else
            {
                lReturn = $('#redirectHost').val().length > 0;
                if (!lReturn)
                    alert("Redirect Host alanını boş geçemezsiniz.");
                else
                {
                    lReturn = $('#sName').val().length > 0;
                    if (!lReturn)
                       alert("sName alanını boş geçemezsiniz.");   
                    else
                    {
                        lReturn = $('#redirectService').val().length > 0;
                        if (!lReturn)
                           alert("Redirect Service alanını boş geçemezsiniz.");
                        else
                        {
                             lReturn = $('#dName').val().length > 0;
                             if (!lReturn)
                                alert("dName alanını boş geçemezsiniz."); 
                            else
                            {
                                lReturn = $('#service').val().length > 0;
                                if (!lReturn)
                                   alert("Service alanını boş geçemezsiniz."); 
                            }
                        }
                    }
                }
            }
            return lReturn;
        }
    </script>
</body>

</html>
