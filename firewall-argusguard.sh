#!/bin/bash

# Kurallar silinir
iptables -t filter -F
iptables -t nat -F
iptables -t mangle -F
iptables -t filter -X
iptables -t nat -X
iptables -t mangle -X

# Moduller yuklenir
depmod -a
modprobe ip_tables
modprobe ip_conntrack
modprobe ip_conntrack_ftp
modprobe ip_conntrack_irc
modprobe iptable_nat
modprobe ip_nat_ftp
modprobe ip_nat_irc

echo "1" > /proc/sys/net/ipv4/ip_forward
echo "1" > /proc/sys/net/ipv4/ip_dynaddr

# DDOS ATAK
iptables -N drop-saldiri
iptables -A drop-saldiri -j LOG --log-level warning --log-prefix "DROPED SYN-FLOOD : "
iptables -A drop-saldiri -j DROP
iptables -N syn-flood 
iptables -A syn-flood -m limit --limit=10/s --limit-burst 24 -j RETURN 
iptables -A syn-flood -j drop-saldiri

# GENEL IZINLER
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT
iptables -A OUTPUT -o eth0 -j ACCEPT
iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT

##  SUBNET'LER  ##############################################
##############################################################
LAN="10.10.10.0/24"
EXT_IF="eth0"

##  SUBNET NAT  ##############################################
##############################################################
iptables -t nat -A POSTROUTING -s $LAN -o $EXT_IF -j MASQUERADE

######################################
####  Local --> internet  ############
######################################
iptables -N DROP_Lan-To-Internet-9
iptables -A DROP_Lan-To-Internet-9 -j LOG --log-level warning --log-prefix "DROP_Lan-To-Internet-9" 
iptables -A DROP_Lan-To-Internet-9 -j DROP
### All --> All
iptables -t nat -A PREROUTING -s 0.0.0.0/0.0.0.0 -d 0.0.0.0/0.0.0.0 -j DROP
iptables -A FORWARD -s 0.0.0.0/0.0.0.0 -d 0.0.0.0/0.0.0.0 -j DROP_Lan-To-Internet-9

######################################
####  internet --> Local  ############
######################################
iptables -N ACCEPT_Internet-To-Local-4
iptables -A ACCEPT_Internet-To-Local-4 -j LOG --log-level warning --log-prefix "ACCEPT_Internet-To-Local-4" 
iptables -A ACCEPT_Internet-To-Local-4 -j ACCEPT
### All --> eth0 Ext ?nt
iptables -t nat -A PREROUTING -i eth1 -s 0.0.0.0/0.0.0.0 -d 192.168.0.29/255.255.255.255 -p tcp --dport 1522 -j DNAT --to 10.0.0.41:2562
iptables -A FORWARD -i eth1 -d 10.0.0.41 -p udp --dport 2562 -j ACCEPT_Internet-To-Local-4

##############################################
####  Local --> Firewall <-- internet  #######
######################################
iptables -N ACCEPT_Input-Firewall-2
iptables -A ACCEPT_Input-Firewall-2 -j LOG --log-level warning --log-prefix "ACCEPT_Input-Firewall-2" 
iptables -A ACCEPT_Input-Firewall-2 -j ACCEPT
### All --> Firewall
iptables -A INPUT -s 0.0.0.0/0.0.0.0 -p tcp --dport 22 -j ACCEPT_Input-Firewall-2

######################################
iptables -N ACCEPT_Input-Firewall-3
iptables -A ACCEPT_Input-Firewall-3 -j LOG --log-level warning --log-prefix "ACCEPT_Input-Firewall-3" 
iptables -A ACCEPT_Input-Firewall-3 -j ACCEPT
### All --> Firewall
iptables -A INPUT -s 0.0.0.0/0.0.0.0 -p tcp --dport 80 -j ACCEPT_Input-Firewall-3

######################################
iptables -N ACCEPT_Input-Firewall-4
iptables -A ACCEPT_Input-Firewall-4 -j LOG --log-level warning --log-prefix "ACCEPT_Input-Firewall-4" 
iptables -A ACCEPT_Input-Firewall-4 -j ACCEPT
### All --> Firewall
iptables -A INPUT -s 0.0.0.0/0.0.0.0 -p tcp --dport 9999 -j ACCEPT_Input-Firewall-4

######################################
iptables -N DROP_Input-Firewall-5
iptables -A DROP_Input-Firewall-5 -j LOG --log-level warning --log-prefix "DROP_Input-Firewall-5" 
iptables -A DROP_Input-Firewall-5 -j DROP
### All --> Firewall
iptables -A INPUT -s 0.0.0.0/0.0.0.0 -j DROP_Input-Firewall-5

