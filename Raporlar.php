<!DOCTYPE html>
<html lang="tr">
    <head>
        <?php
        include './resource/MetaTitleLink.php';
        include './resource/servisler.php';
        include './resource/Veritabani.php';

        $renkler = array(
            array("#F7464A", "#FF5A5E"),
            array("#46BFBD", "#5AD3D1"),
            array("#FDB45C", "#FFC870"),
            array("#949FB1", "#A8B3C5"),
            array("#4D5360", "#616774")
        );
        ?>
    <script src="resource/js/Chart.js" type="text/javascript"></script>
</head>
<body>
<div id="wrapper">
    <?php
    $mSelect = 2;
    include './resource/NavBar.php';
    ?> 
    <div id="page-wrapper">
        <!--BURADAN SONRA İÇERİK GİRİLEBİLİR-->
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div  class="panel panel-info">
                    <div class="panel-heading">Son 30 Günün Trafik Bilgileri</div>
                    <div class="panel-body"  >
                        <div class="panel-body" style="padding: 0px">
                            <canvas id="trafikBilgileri" height="200" width="600"></canvas>
                            <?php
                            $sayac = 0;
                            $trafikBilgileriBasliklar = "";
                            $trafikBilgileriDegerler = "";
                            foreach ($Connection->query(
                                    '   SELECT EXTRACT(DAY FROM l.date) gun, TRUNCATE( SUM(l.size)/100/100 ,0) kullanim FROM alllogs l  ' .
                                    '    WHERE l.date >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH) AND l.date <= CURDATE() ' .
                                    ' GROUP BY l.date ' .
                                    ' ORDER BY l.date ') as $row) {
                                $trafikBilgileriBasliklar .= ($sayac == 0 ? "" : ",") . '"' . $row["gun"] . '"';
                                $trafikBilgileriDegerler .= ($sayac == 0 ? "" : ",") . $row["kullanim"];
                                $sayac++;
                            }
                            ?>
                            <script>
                                var dataTrafikBilgileri = {
                                    labels: [<?php echo $trafikBilgileriBasliklar; ?>],
                                    datasets: [
                                        {
                                            label: "My Second dataset",
                                            fillColor: "rgba(43,187,205,0.2)",
                                            strokeColor: "rgba(43,187,205,1)",
                                            pointColor: "rgba(43,187,205,1)",
                                            pointStrokeColor: "#fff",
                                            pointHighlightFill: "#fff",
                                            pointHighlightStroke: "rgba(151,187,205,1)",
                                            data: [<?php echo $trafikBilgileriDegerler; ?>]
                                        }
                                    ]
                                }
                            </script>
                        </div>
                    </div>
                </div>
            </div>



            <div class="row" style="padding: 15px;">
                <div class="col-lg-6">
                    <div  class="panel panel-info">
                        <div class="panel-heading">En Çok Veri Transferi Yapan Kullanicilar</div>
                        <div class="panel-body" >
                            <table class="table table-hover" id="dataTables-example">
                                <tbody>
                                    <?php
                                    $sayac = 0;
                                    $max = 100;
                                    $oran = 0;
                                    foreach ($Connection->query(
                                            '    SELECT COALESCE(h.name, g.user) user, g.kullanim ' .
                                            '      FROM ( ' .
                                            '    SELECT l.user, TRUNCATE( SUM(l.size)/100/100 ,0) AS kullanim  ' .
                                            '      FROM alllogs l  ' .
                                            '     WHERE l.date >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH) AND l.date <= CURDATE()  ' .
                                            '  GROUP BY l.user  ' .
                                            '     LIMIT 10 ' .
                                            '           ) g ' .
                                            ' LEFT JOIN host h ON h.address = g.user ' .
                                            '  ORDER BY 2 DESC ') as $row) {
                                        $sayac++;
                                        if ($sayac == 1)
                                            $max = $row['kullanim'];
                                        $oran = floor(100 / $max * $row['kullanim']);
                                        echo
                                        ' <tr> ' .
                                        '   <td style="width: 5%">' . $sayac . '</td> ' .
                                        '   <td style="width: 55%">' . $row['user'] . '</td> ' .
                                        '   <td style="width: 25%"> ' .
                                        '       <div class="progress" style="margin: 0;"><div class="progress-bar progress-bar-info" style="width: ' . $oran . '%"></div></div> ' .
                                        '   </td> ' .
                                        '   <td style="width: 15%; text-align: right">' . $row['kullanim'] . ' mb</td> ' .
                                        ' </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div  class="panel panel-info">
                        <div class="panel-heading">En Çok Bağlantı Yapan Kullanicilar</div>
                        <div class="panel-body"  >
                            <table class="table table-hover" id="dataTables-example">
                                <tbody>
                                    <?php
                                    $sayac = 0;
                                    $max = 100;
                                    $oran = 0;
                                    foreach ($Connection->query(
                                            '    SELECT COALESCE(h.name, g.user) user, g.kullanim ' .
                                            '      FROM ( ' .
                                            '    SELECT l.user, count(*) kullanim  ' .
                                            '      FROM alllogs l  ' .
                                            '     WHERE l.date >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH) AND l.date <= CURDATE()  ' .
                                            '  GROUP BY l.user  ' .
                                            '     LIMIT 10 ' .
                                            '           ) g ' .
                                            ' LEFT JOIN host h ON h.address = g.user ' .
                                            '  ORDER BY 2 DESC ') as $row) {
                                        $sayac++;
                                        if ($sayac == 1)
                                            $max = $row['kullanim'];
                                        $oran = floor(100 / $max * $row['kullanim']);
                                        echo
                                        ' <tr> ' .
                                        '   <td style="width: 5%">' . $sayac . '</td> ' .
                                        '   <td style="width: 55%">' . $row['user'] . '</td> ' .
                                        '   <td style="width: 25%"> ' .
                                        '       <div class="progress" style="margin: 0;"><div class="progress-bar progress-bar-info" style="width: ' . $oran . '%"></div></div> ' .
                                        '   </td> ' .
                                        '   <td style="width: 15%; text-align: right">' . $row['kullanim'] . ' </td> ' .
                                        ' </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row" style="padding: 15px;">
                <div class="col-lg-6">
                    <div  class="panel panel-info">
                        <div class="panel-heading">En Çok Girilen Siteler</div>
                        <div class="panel-body" >
                            <table class="table table-hover" id="dataTables-example">
                                <tbody>
                                    <?php
                                    $sayac = 0;
                                    $max = 100;
                                    $oran = 0;
                                    foreach ($Connection->query(
                                            '   SELECT l.Domain, count(*) AS kullanim  ' .
                                            '      FROM alllogs l  ' .
                                            '     WHERE l.date >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH) AND l.date <= CURDATE()  ' .
                                            '  GROUP BY l.Domain  ' .
                                            '  ORDER BY 2 DESC ' .
                                            '     LIMIT 10') as $row) {
                                        $sayac++;
                                        if ($sayac == 1)
                                            $max = $row['kullanim'];
                                        $oran = floor(100 / $max * $row['kullanim']);
                                        echo
                                        ' <tr> ' .
                                        '   <td style="width: 5%">' . $sayac . '</td> ' .
                                        '   <td style="width: 55%">' . $row['Domain'] . '</td> ' .
                                        '   <td style="width: 25%"> ' .
                                        '       <div class="progress" style="margin: 0;"><div class="progress-bar progress-bar-info" style="width: ' . $oran . '%"></div></div> ' .
                                        '   </td> ' .
                                        '   <td style="width: 15%; text-align: right">' . $row['kullanim'] . ' </td> ' .
                                        ' </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div  class="panel panel-info">
                        <div class="panel-heading">En Çok Veri Transferi Yapan Siteler</div>
                        <div class="panel-body"  >
                            <table class="table table-hover" id="dataTables-example">
                                <tbody>
                                    <?php
                                    $sayac = 0;
                                    $max = 100;
                                    $oran = 0;
                                    foreach ($Connection->query(
                                            '   SELECT l.Domain, TRUNCATE( SUM(l.size)/100/100 ,0) AS kullanim  ' .
                                            '      FROM alllogs l  ' .
                                            '     WHERE l.date >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH) AND l.date <= CURDATE()  ' .
                                            '  GROUP BY l.Domain  ' .
                                            '  ORDER BY 2 DESC ' .
                                            '     LIMIT 10') as $row) {
                                        $sayac++;
                                        if ($sayac == 1)
                                            $max = $row['kullanim'];
                                        $oran = floor(100 / $max * $row['kullanim']);
                                        echo
                                        ' <tr> ' .
                                        '   <td style="width: 5%">' . $sayac . '</td> ' .
                                        '   <td style="width: 55%">' . $row['Domain'] . '</td> ' .
                                        '   <td style="width: 25%"> ' .
                                        '       <div class="progress" style="margin: 0;"><div class="progress-bar progress-bar-info" style="width: ' . $oran . '%"></div></div> ' .
                                        '   </td> ' .
                                        '   <td style="width: 15%; text-align: right">' . $row['kullanim'] . ' mb</td> ' .
                                        ' </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-12">
                <div  class="panel panel-info">
                    <div class="panel-heading">Son 30 Günün Trafik Saatleri</div>
                    <div class="panel-body"  >
                        <div class="panel-body" style="padding: 0px">
                            <canvas id="trafikSaatleri" height="200" width="600"></canvas>
                            <?php
                            $sayac = 0;
                            $trafikBilgileriBasliklar = "";
                            $trafikBilgileriDegerler = "";
                            foreach ($Connection->query(
                                    '   SELECT EXTRACT(hour FROM l.time) saat, TRUNCATE( SUM(l.size)/100/100 ,0) kullanim FROM alllogs l  ' .
                                    '    WHERE l.date >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH) AND l.date <= CURDATE() ' .
                                    ' GROUP BY EXTRACT(hour FROM l.time) ' .
                                    ' ORDER BY EXTRACT(hour FROM l.time) ') as $row) {
                                $trafikBilgileriBasliklar .= ($sayac == 0 ? "" : ",") . '"' . $row["saat"] . '"';
                                $trafikBilgileriDegerler .= ($sayac == 0 ? "" : ",") . $row["kullanim"];
                                $sayac++;
                            }
                            ?>
                            <script>
                                var dataTrafikSaatleri = {
                                    labels: [<?php echo $trafikBilgileriBasliklar; ?>],
                                    datasets: [
                                        {
                                            label: "Son 30 Günün Trafik Saatleri",
                                            fillColor: "rgba(43,187,205,0.2)",
                                            strokeColor: "rgba(43,187,205,1)",
                                            pointColor: "rgba(43,187,205,1)",
                                            pointStrokeColor: "#fff",
                                            pointHighlightFill: "#fff",
                                            pointHighlightStroke: "rgba(151,187,205,1)",
                                            data: [<?php echo $trafikBilgileriDegerler; ?>]
                                        }
                                    ]
                                }
                            </script>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row" style="padding: 15px;">
                <div class="col-lg-6">
                    <div  class="panel panel-info">
                        <div class="panel-heading">En Çok Bloklanan Siteler</div>
                        <div class="panel-body" >
                            <table class="table table-hover" id="dataTables-example">
                                <tbody>
                                    <?php
                                    $sayac = 0;
                                    $max = 100;
                                    $oran = 0;
                                    foreach ($Connection->query(
                                            '   SELECT l.Domain, COUNT(*) kullanim  ' .
                                            '     FROM alllogs l  ' .
                                            '    WHERE l.date >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH) AND l.date <= CURDATE() ' .
                                            '      AND SUBSTRING(l.Action,2,POSITION(\'* \' IN l.Action)-2) = \'DENIED\' ' .
                                            ' GROUP BY l.domain ' .
                                            ' ORDER BY 2 DESC ' .
                                            '    LIMIT 10 ') as $row) {
                                        $sayac++;
                                        if ($sayac == 1)
                                            $max = $row['kullanim'];
                                        $oran = floor(100 / $max * $row['kullanim']);
                                        echo
                                        ' <tr> ' .
                                        '   <td style="width: 5%">' . $sayac . '</td> ' .
                                        '   <td style="width: 55%">' . $row['Domain'] . '</td> ' .
                                        '   <td style="width: 25%"> ' .
                                        '       <div class="progress" style="margin: 0;"><div class="progress-bar progress-bar-info" style="width: ' . $oran . '%"></div></div> ' .
                                        '   </td> ' .
                                        '   <td style="width: 15%; text-align: right">' . $row['kullanim'] . ' </td> ' .
                                        ' </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div  class="panel panel-info">
                        <div class="panel-heading">Filtreleme İstatistiği</div>
                        <div class="panel-body"  style="padding: 20px 60px;" >
                            <div class="panel-body" style="padding: 0px; max-width: 400px; margin: auto" >
                                <?php
                                $sayac = 0;
                                $filtreIstatistikleriVeri = "";
                                foreach ($Connection->query(
                                        '   SELECT SUBSTRING(l.Action,2,position(\'* \' in l.Action)-2) durum, count(*) kullanim  ' .
                                        '     FROM alllogs l  ' .
                                        '    WHERE l.date >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH) AND l.date <= CURDATE() ' .
                                        ' GROUP BY SUBSTRING(l.Action,2,position(\'* \' in l.Action)-2) ') as $row) {
                                    $filtreIstatistikleriVeri .= ($sayac == 0 ? "" : ",") .
                                            '{ ' .
                                            '    value: ' . $row["kullanim"] . ', ' .
                                            '    color: "' . $renkler[$sayac][0] . '", ' .
                                            '    highlight: "' . $renkler[$sayac][1] . '", ' .
                                            '    label: "' . $row["durum"] . '" ' .
                                            '}';
                                    $sayac++;
                                }
                                ?>
                                <canvas id="filtreIstatistigi" width="500" height="500"/>
                                <script>
                                    var dataFiltreIstatistigi = [<?php echo $filtreIstatistikleriVeri; ?>];
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




        </div>
        <br><br>
        <!--BURADAN ÖNCE İÇERİK GİRİLEBİLİR-->
    </div>
</div>
<?php include './resource/EndScript.php'; ?> 
<script>

    window.onload = function () {
        window.myLine = new Chart(document.getElementById("trafikBilgileri").getContext("2d")).Line(dataTrafikBilgileri, {responsive: true});
        window.myLine2 = new Chart(document.getElementById("trafikSaatleri").getContext("2d")).Line(dataTrafikSaatleri, {responsive: true});
        window.myDoughnut = new Chart(document.getElementById("filtreIstatistigi").getContext("2d")).Doughnut(dataFiltreIstatistigi, {responsive: true});
    }
</script>
</body>

</html>
