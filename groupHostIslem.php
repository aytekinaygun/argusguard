<?php

include './resource/Veritabani.php';

if ($_GET) {
    $hots = htmlspecialchars($_GET["h"]);
    $group = htmlspecialchars($_GET["g"]);
    $islem = htmlspecialchars($_GET["i"]);


    if ($islem == 1) {
        $query = $Connection->prepare('INSERT INTO groupitem (`host`, `group`) VALUES (:host, :group );');
        $query->execute(array('host' => $hots, 'group' => $group));
    } else {
        $query = $Connection->prepare('DELETE FROM groupitem WHERE `host` = :host AND `group` = :group;');
        $query->execute(array('host' => $hots, 'group' => $group));
    }

    $Connection = NULL;
}
header('Location:group.php?id=' . $group);
exit();
?>