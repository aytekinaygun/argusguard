<!DOCTYPE html>
<html lang="tr">
    <head>
        <?php include './resource/MetaTitleLink.php'; ?> 
    </head>
    <body>
    <div id="wrapper">
        <?php
        $mSelect = 22;
        include './resource/NavBar.php';
        include './resource/Veritabani.php';

        $id = 0;
        $name = "";
        $comment = "";
        if ($_GET) {
            $id = $_GET['id'];

            $row = $Connection->query('SELECT * FROM hostgroup WHERE id = ' . $id)->fetch();
            if ($row) {
                $id = $row["id"];
                $name = $row["name"];
                $comment = $row["comment"];
            } else {
                header('Location:MerkezGroups.php');
                exit();
            }
        }
        ?> 
        <div id="page-wrapper">
            <!--BURADAN SONRA İÇERİK GİRİLEBİLİR-->
            <br/>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Merkezi Group Bilgisi
                            <div style="text-align: right; position: static; float: right">
                                <?php
                                if ($id > 0)
                                    echo '<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#myModal">Merkezi Grubu Sil</button>';
                                ?>
                            </div>
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Dikkat</h4>
                                        </div>
                                        <div class="modal-body">
                                            Merkezi Group kaydını silmek istediğinizden emin misiniz?
                                        </div>
                                        <div class="modal-footer">

                                            <a href = "MerkezGroupKaydet.php?silId=<?php echo $id; ?>">
                                                <button type = "button" class = "btn btn-danger" >Sil</button>
                                            </a>
                                            <button type = "button" class = "btn btn-default" data-dismiss = "modal">Hayır</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class = "panel-body">
                            <form id = "form1" name = "form1" role = "form" action = "MerkezGroupKaydet.php" method = "POST" >
                                <div class = "row">
                                    <input name = "id" type = "hidden" value = "<?php echo $id . ''; ?>" />
                                    <div class = "col-lg-6">
                                        <div class = "form-group">
                                            <label>Name</label>
                                            <input name = "name" class = "form-control" placeholder = "Name" value = "<?php echo $name ?>"/>
                                        </div>
                                    </div>
                                    <div class = "col-lg-6">
                                        <div class="form-group">
                                            <label>Comment</label>
                                            <input name="comment" class="form-control" placeholder="Comment" value="<?php echo $comment; ?>">
                                        </div>
                                    </div>
                                    <br/>
                                    <button style=" float: right; margin-right: 15px; width: 120px" type="submit" class="btn btn-primary" ><i class="fa fa-save"></i> Kaydet</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <?php if ($id > 0) { ?>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="margin-bottom: 10px" >
                                Gruba Dahil Olan Hostlar                      
                            </div>
                            <table class="table table-striped table-hover" id="grubaDahilOlanlar">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Comment</th>
                                        <th style="width: 50px">#</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $Query = $Connection->prepare('
                                          SELECT h.id, h.name, h.address 
                                            FROM host h
                                           WHERE h.tur = 1 and h.id IN (SELECT i.host FROM groupitem i WHERE i.group = :group )
                                        ORDER BY h.name
                                        ');
                                    $Query->execute(array('group' => $id));
                                    foreach ($Query as $row) {
                                        echo '
                                        <tr  class="infdo">
                                            <td>' . $row['name'] . '</td>
                                            <td>' . $row['address'] . '</td>
                                            <td>
                                                <a href="MerkezGroupHostIslem.php?h=' . $row['id'] . '&g=' . $id . '&i=0">
                                                <button type="button" style="float: right;" class="btn btn-outline btn-danger btn-xs">Sil<i class="fa fa-chevron-right"></i></button>
                                                </a>
                                            </td>
                                        </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="margin-bottom: 10px" >
                                Gruba Dahil Olmayan Diğer Hostlar
                            </div>
                            <table class="table table-striped table-hover" id="grubaDahilOlmayanlar">
                                <thead>
                                    <tr>
                                        <th style="width: 50px">#</th>
                                        <th>Name</th>
                                        <th>Comment</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $Query = $Connection->prepare('
                                          SELECT h.id, h.name, h.address 
                                            FROM host h
                                           WHERE h.tur = 1 and h.id NOT IN (SELECT i.host FROM groupitem i WHERE i.group = :group )
                                        ORDER BY h.name
                                        ');
                                    $Query->execute(array('group' => $id));
                                    foreach ($Query as $row) {
                                        echo '
                                        <tr class="succedss">
                                            <td>
                                                <a href="MerkezGroupHostIslem.php?h=' . $row['id'] . '&g=' . $id . '&i=1">
                                                <button type="button" class="btn btn-outline btn-primary btn-xs"><i class="fa fa-chevron-left"></i>Ekle</button>
                                                </a>
                                            </td>
                                            <td>' . $row['name'] . '</td>
                                            <td>' . $row['address'] . '</td>
                                        </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <!--BURADAN ÖNCE İÇERİK GİRİLEBİLİR-->
        </div>
    </div>
    <?php include './resource/EndScript.php'; ?>  
    <script>
        $(document).ready(function () {
            $('#grubaDahilOlmayanlar').dataTable();
            $('#grubaDahilOlanlar').dataTable();
        });
    </script>
</body>

</html>
