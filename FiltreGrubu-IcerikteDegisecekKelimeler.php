<!DOCTYPE html>
<html lang="tr">
    <head>
        <?php
        include './resource/MetaTitleLink.php';
        include './resource/dosyaYollari.php';
        include './resource/DosyaIslemleri.php';
        include './resource/Veritabani.php';
        include './resource/Araclar.php';

        $id = GETT('id', 0);
        if ($id == 0)
            $id = POSTT('id', 0);
        $DosyaYolu = $DansguardianKlasoru . 'lists/filtergroups' . $id . '/contentregexplist';

        if ($_POST) {
            $veri = POSTT('veri', '');
            file_put_contents($DosyaYolu, $veri);
            header('Location:FiltreGrubu-TemelKurallar.php?id=' . $id);
            exit();
        }

        if (!$_GET) {
            header('Location:FiltreGruplari.php');
            exit();
        }
        $GrupAdi = GrupAdiGetir($DansguardianKlasoru, $id);
        ?> 
    </head>
    <body>
    <div id="wrapper">
        <?php
        $mSelect = 53;
        include './resource/NavBar.php';
        ?> 
        <div id="page-wrapper">
            <!--BURADAN SONRA İÇERİK GİRİLEBİLİR-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="panel panel-default" style="margin-bottom: 15px; margin-top: -20px;">
                            <div class="panel-heading">
                                <h3 style="margin-bottom: 0;"><?php echo $GrupAdi; ?></h3>
                            </div>
                            <div class="panel-body" style="padding: 3px;">
                                <?php
                                $FiltreGrubuMenu = 8;
                                include './FiltreGrubuMenu.php';
                                ?>   
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="row">
                                <form id = "f1" name="f1" role="form" action="FiltreGrubu-IcerikteDegisecekKelimeler.php" method="POST" >
                                    <div  class="panel panel-info">
                                        <div class="panel-heading">
                                            İçerikte Değişecek Kelimeler
                                            <button type="submit" class="btn btn-info" 
                                                    style="margin: -7px 0px; float: right; width: 142px;" >
                                                <i class="fa fa-save"></i> Kaydet
                                            </button>
                                        </div>
                                        <div class="panel-body" >
                                            <input name="id" type="hidden" value="<?php echo $id . ""; ?>" />
                                            <textarea name="veri" class="form-control col-lg-12" rows="21"><?php echo file_get_contents($DosyaYolu); ?></textarea>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--BURADAN ÖNCE İÇERİK GİRİLEBİLİR-->
        </div>
    </div>
    <?php include './resource/EndScript.php'; ?> 
</body>

</html>
