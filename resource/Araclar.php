<?php

function GETT($Parametre, $Varsayilan) {
    if (!empty($_GET[$Parametre])) {
        return htmlspecialchars_decode($_GET[$Parametre]);
    } else {
        return $Varsayilan;
    }
}

function POSTT($Parametre, $Varsayilan) {
    if (!empty($_POST[$Parametre])) {
        return htmlspecialchars_decode($_POST[$Parametre]);
    } else {
        return $Varsayilan;
    }
}

function POSTT_ON_ARKA($Parametre, $Varsayilan, $OnEk, $ArkaEk) {
    $Veri = POSTT($Parametre, $Varsayilan);
    if (strlen($Veri) > 0)
        return $OnEk . $Veri . $ArkaEk;
    return $Veri;
}

function StrTemizle($String) {
    return $String;
    //return str_replace("\\", "\\\\",str_replace("'", "\'", str_replace('"', '&quot;', $String)));
}
