<html>
    <head>
        <?php include './resource/MetaTitleLink.php'; ?> 
    </head>
    <body>
        <?php
        include './resource/Veritabani.php';

        if ($_POST) {

            $Kullanici = htmlspecialchars($_POST['kullanici']);
            $Parola = htmlspecialchars($_POST['parola']);

            $Query = $Connection->prepare("SELECT * FROM kullanicilar WHERE user_name = :name ");
            $Query->execute(array('name' => $Kullanici));
            foreach ($Query as $row) {
                if ($row['password'] == $Parola) {
                    session_start();
                    $_SESSION['oturum'] = TRUE;
                    $_SESSION['user_id'] = $row['id'];
                    $_SESSION['full_name'] = $row['full_name'];
                    $_SESSION['giris_zamani'] = date("d.m.Y H:i:s") . "";
                    $_SESSION['merkez_kullanicisi'] = $row['merkez_kullanicisi'] == 1;
                    header("Location: index.php");
                    exit();
                }
            }
            echo "<script> alert('Kullanıcı adı veya parola yanlış'); </script>";
        }
        ?>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title" style="text-align: center"><img src="resource/img/ag.png" alt=""/></h3>

                    </div>
                    <div class="panel-body">
                        <form role="form" method="POST" action="giris.php">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Kullanını Adı" name="kullanici" type="text" autofocus/>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Parola" name="parola" type="password" value=""/>
                                </div>
                                <input type="submit" class="btn btn-lg btn-outline btn-success btn-block" value="Giriş"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include './resource/EndScript.php'; ?> 
</body>
</html>
