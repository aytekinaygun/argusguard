<?php

include './resource/dosyaYollari.php';
include './resource/Araclar.php';
include './resource/DosyaIslemleri.php';
include './resource/Veritabani.php';

$id = GETT("id", 0);

if ($id > 0) {
    unlink($DansguardianKlasoru . 'dansguardianf' . $id . '.conf');

    function klasorSil($klasor) {
        if ($objs = glob($klasor . "/*")) {
            foreach ($objs as $obj) {
                is_dir($obj) ? rmdirr($obj) : unlink($obj);
            }
        }
        rmdir($klasor);
    }

    klasorSil($DansguardianKlasoru . 'lists/filtergroups' . $id);


    $GrupSayisi = DosyadanBilgiOku($DansguardianKlasoru . 'dansguardian.conf', 'filtergroups', 0) - 1;
    DosyadanBilgiDegistir($DansguardianKlasoru . 'dansguardian.conf', array('filtergroups'), array($GrupSayisi));

    $Connection->exec('DELETE FROM filtre_kategori_secim WHERE grup = ' . $id);

    $Connection->exec('DELETE FROM filtre_kullanicilar WHERE filtre_grubu = ' . $id);
    include './FiltreGrubuKullanicilarDosyasiOlustur.php';

    $Connection = null;
}

header('Location:FiltreGruplari.php');
exit();
