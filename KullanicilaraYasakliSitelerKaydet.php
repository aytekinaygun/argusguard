<?php

include './resource/Veritabani.php';
include './resource/dosyaYollari.php';

if ($_POST) {
    $name = htmlspecialchars_decode($_POST["name"]);
    $oldName = htmlspecialchars_decode($_POST["oldName"]);
    $silName = htmlspecialchars_decode($_POST["silName"]);
    $aktif = htmlspecialchars_decode($_POST["aktif"]);

    $fh = fopen($DansguardianKlasoru . 'lists/bannedsitelist-all', 'r+');
    $YeniDosya = '';

    while (!feof($fh)) {
        $lSatir = trim(fgets($fh));
        if (strlen($lSatir) > 0) {
            if ((strlen(trim($silName)) == 0 ) || !(trim($lSatir) == trim($silName) || trim($lSatir) == trim('#' . $silName))) {
                if ((strlen(trim($oldName)) > 0 ) && (trim($lSatir) == trim($oldName) || trim($lSatir) == trim('#' . $oldName)))
                    $YeniDosya .= ($aktif ? '' : '#') . trim($name);
                else
                    $YeniDosya .= trim($lSatir);
                $YeniDosya .= "\r\n";
            }
        }
    }
    if (strlen(trim($oldName)) == 0) {
        $YeniDosya .= trim($name);
        $YeniDosya .= "\r\n";
    }

    file_put_contents($DansguardianKlasoru . 'lists/bannedsitelist-all', $YeniDosya);
    fclose($fh);
}
header('Location:KullanicilaraYasakliSiteler.php');
exit();
?>
